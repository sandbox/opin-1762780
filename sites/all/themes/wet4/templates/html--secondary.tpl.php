<?php require_once('inc/html-head.inc'); ?>
<body class="secondary" vocab="http://schema.org/" typeof="WebPage">
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php require_once('inc/html-foot.inc'); ?>
</body>

</html>
