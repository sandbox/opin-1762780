<?php // $Id$

/**
 * @file template.process-page.inc
 */

/**
 * Override or insert variables into page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called.
 */

global $theme, $theme_info, $user;


// Set local tasks variables
$vars['primary_local_tasks'] = menu_primary_local_tasks();
$vars['secondary_local_tasks'] = menu_secondary_local_tasks();
