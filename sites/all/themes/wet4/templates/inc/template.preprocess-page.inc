<?php // $Id$

/**
 * @file template.preprocess-page.inc
 */

/**
 * Override or insert variables into page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called.
 */

global $theme, $theme_info, $user;

/* Add a page.tpl.php depending on content type. */
if (isset($vars['node']->type)) {
  // If the content type's machine name is "my_machine_name" the file
  // name will be "page--my-machine-name.tpl.php".
  $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
}

