<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MG4LMJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MG4LMJ');</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TLGQ9K" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer1'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer1','GTM-TLGQ9K');</script>
<ul id="wb-tphp">
  <li class="wb-slc"><a class="wb-sl" href="#wb-cont"><?php print t('Skip to main content');?></a></li>
  <li class="wb-slc visible-md visible-lg"><a class="wb-sl" href="#wb-info"><?php print t('Skip to site information');?></a></li>
</ul>
<header role="banner">
<div id="wb-bnr" class="container">
<section id="wb-lng" class="visible-md visible-lg text-right">
<h2 class="wb-inv">Language selection</h2>
<div class="row">
<div class="col-md-12">
<?php echo print_r(wet4_language_lang_switcher(),true); ?>
</div>
</div>
</section>
<div class="row">
<div class="brand col-xs-9 col-md-6"><a href="/en/home">
<!--<object type="image/svg+xml" tabindex="-1" role="img" data="/<?php echo drupal_get_path('theme', 'wet4'); ?>/img/headers/TheSource.svg" aria-label="The Source - NRCan's Intranet"></object>-->
<img src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/img/headers/TheSource.svg" alt="The Source" />
</a></div>
<section class="wb-mb-links col-xs-3 visible-sm visible-xs" id="wb-glb-mn" >
<h2>Menu</h2>
<ul class="list-inline text-right chvrn">
<li><a href="#mb-pnl" title="Menu" aria-controls="mb-pnl" class="overlay-lnk" role="button"><span class="glyphicon glyphicon-th-list"><span class="wb-inv">Menu</span></span></a></li>
</ul>
<div id="mb-pnl"></div>
</section>
<?php
// Include search template.
include_once(drupal_get_path('theme', 'wet4') .'/templates/blocks/search-block-form.tpl.php');
?>
</div>
</div>

<nav role="navigation" id="wb-sm" class="wb-menu visible-md visible-lg" data-trgt="mb-pnl" data-ajax-fetch="/<?php echo drupal_get_path('theme', 'wet4'); ?>/templates/inc/menus/sitemenu-en.html" typeof="SiteNavigationElement">
<h2 class="wb-inv">Site menu</h2>
<div class="container visible-md visible-lg nvbar">
<div class="row">
<ul class="list-inline menu">
<li><a href="/services">Services &amp; Policies</a></li>
<li><a href="/tools-templates">Tools &amp; Templates</a></li>
<li><a href="/news">News &amp; Events</a></li>
<li><a href="/connect-share">Connect & Share</a></li>
<li><a href="/department">The Department</a></li>
</ul>
</div>
</div>
</nav>
 <!--<?php if ($breadcrumb): print $breadcrumb; endif;?>-->
</header>
