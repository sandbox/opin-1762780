<?php // $Id$

/**
 * @file template.preprocess-html.inc
 */

/**
 * Override or insert variables into page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called.
 */

global $theme, $theme_info, $user;

if ($node = menu_get_object()) {
  if (isset($node->type)) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "hmtl--my-machine-name.tpl.php".
    $vars['theme_hook_suggestions'][] = 'html__' . $node->type;
  }
}


