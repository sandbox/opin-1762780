<?php // $Id$

/**
 * @file template.theme-overrides.inc
 */

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */

// Ouptuts site breadcrumbs with current page title appended onto trail
function wet4_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users.
    $output = '<nav role="navigation" id="wb-bc" class="" property="breadcrumb">';
    $output .= '<h2>' . t('You are here') . '</h2>';
    $output .= '<div class="container"><div class="row"><ol class="breadcrumb">';

      $crumbs = '';
      $array_size = count($breadcrumb);
      $i = 0;
      while ( $i < $array_size) {
        $crumbs .= '<li>' . $breadcrumb[$i] . '</li>';
        $i++;
      }
      $crumbs .= '<li>'. drupal_get_title() .'</li>';

    $output .= $crumbs;
    $output .= '</ol></div></div></nav>';
    return $output;
  }
}

/* Only use theme-layer css for logged-out users,
* remove module-layer and system-layer css
* ================================================
* $value['group'] = 100 = CSS_THEME = (Theme-Layer CSS)
* $value['group'] = 0 = CSS_DEFAULT = (Module-Layer CSS)
* $value['group'] = -100 = CSS_SYSTEM = (System-Layer CSS) */
function wet4_css_alter(&$css) {
  foreach ($css as $key => $value) {
    if ($value['group'] != CSS_THEME) {
      $exclude[$key] = FALSE;
    }
  }
  if (!(bool)$GLOBALS['user']->uid){
    $css = array_diff_key($css, $exclude);
  }
}

/* Only use theme-layer js for logged-out users,
* remove module-layer and system-layer js
* ================================================
* $value['group'] = 100 = JS_THEME = (Theme-Layer JS)
* $value['group'] = 0 = JS_DEFAULT = (Module-Layer JS)
* $value['group'] = -100 = JS_SYSTEM = (System-Layer JS) */
function wet4_js_alter(&$javascript) {
  #echo print_r($javascript);
  foreach ($javascript as $key => $value) {
    if ($value['group'] != JS_THEME) {
      $exclude[$key] = FALSE;
    }
  }
  if (!(bool)$GLOBALS['user']->uid){
    $javascript = array_diff_key($javascript, $exclude);
  }
}

function wet4_menu_tree(&$variables) {
  return '<ul class="list-group menu list-unstyled">' . $variables['tree'] . '</ul>';
}

function wet4_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = wet4_l($element['#title'], $element['#href'], array('attributes' => array('class' => array('list-group-item'))));
  return '<li>' . $output . $sub_menu . "</li>\n";
}

/**
 * Return the URL language switcher block. Translation links may be provided by
 * other modules.
 */
function wet4_language_switcher_url($type, $path) {
  $languages = language_list('enabled');
  $links = array();

  foreach ($languages[1] as $language) {
    $links[$language->language] = array(
      'href'       => $path,
      'title'      => $language->native,
      'language'   => $language,
      'attributes' => array('class' => array('language-link')),
    );
  }

  return $links;
}

/**
 * Implements _wetkit_language_lang_switcher().
 */
function wet4_language_lang_switcher() {
  global $language;
  $term = '';
  $language_paths = '';
  $language_terms = '';

  $languages = language_list('enabled');
  $path = drupal_is_front_page() ? '<front>' : $_GET['q'];

  // Handle node translations.
  if (arg(0) == 'node' && arg(1) > 0) {
    $node_path = 'node/' . arg(1);
    $language_paths = translation_path_get_translations($node_path);
  }

  // Handle term translations.
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && arg(2) > 0) {
    $term = taxonomy_get_term_by_name(arg(2));
    $language_terms = i18n_taxonomy_term_get_translation(array('tid' => arg(2)), $language->language);
  }

  // Handle all languages.
  if (count($languages[1]) > 1) {
    $lang_switcher = array();
    foreach ($languages[1] as $lang) {
      if ($lang->language != $language->language) {
        $modifier = $lang->native;
        // We are translating a node so will check if missing translations.
        if ($language_paths && !empty($language_paths[$lang->language])) {
          $path = $language_paths[$lang->language];
        }
        // We are translating a term.
        if ($term) {
          $path = 'taxonomy/term/' . $language_terms[$lang->language]->tid;
        }
        #@TODO: Add title attribute referencing to the title of the node
        $attributes = array('lang' => $lang->language);
        $lang_switcher[] = '<li>' . l($modifier, $path, array('attributes' => $attributes, 'language' => $lang)) . '</li>';
      }
    }
    return '<ul class="list-inline margin-bottom-none">' . implode($lang_switcher) . '</ul>';
  }
}

function wet4_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Search the website'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 40;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('Search'); // Change the text on the submit button
    $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');
    // Add extra attributes to the text box
    //$form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    //$form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
    $form['#attributes']['class'] = "form-inline";
    $form['#attributes']['name'] = "cse-search-box";
    $form['#attributes']['id'] = "cse-search-box";
    $form['#attributes']['method'] = "get";
    $form['#attributes']['role'] = "search";
    // Prevent user from searching the default text
    //$form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";
    // Alternative (HTML5) placeholder attribute instead of using the javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  }
}

function wet4_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="wb-inv">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  return '<li' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>' . l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
}
