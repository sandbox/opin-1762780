<?php #@todo: Create a content type and a view for features ?>
<aside class="features">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Features</h2>
</div>
</div>
<div class="wb-eqht">
<a href="#" class="sect-lnks thumbnail" rel="external"><figure>
<img src="/sites/all/themes/wet4/img/features/blueprint_e.jpg" alt="">
<figcaption>Blueprint 2020</figcaption>
</figure></a>
<a href="#" class="sect-lnks thumbnail" rel="external"><figure>
<img src="/sites/all/themes/wet4/img/features/GCDocs.jpg" alt="" >
<figcaption>GCDocs</figcaption>
</figure></a>
<a href="#" class="sect-lnks thumbnail" rel="external"><figure>
<img src="/sites/all/themes/wet4/img/features/2014_survey_e.jpg" alt="" >
<figcaption>2014 Survey</figcaption>
</figure></a>
</div>
</div>
</aside>
<footer role="contentinfo" id="wb-info">
<nav role="navigation" class="container visible-sm visible-md visible-lg wb-navcurr">

<h2 class="wb-inv">Site Information</h2>
<div class="row">
<section class="col-sm-3 col-lg-3">
<h3>Government</h3>
<ul class="list-unstyled">
<li><a href="/en/gov/system/index.html">How government works</a></li>
<li><a href="/en/gov/dept/index.html">Departments &amp; agencies</a></li>
<li><a href="http://pm.gc.ca/eng" rel="external">Prime Minister</a></li>
<li><a href="/en/gov/ministers/index.html">Ministers</a></li>
<li><a href="/en/gov/policy/index.html">Policies, regulations &amp; laws</a></li>
<li><a href="/en/gov/libraries/index.html">Libraries</a></li>
<li><a href="/en/gov/publications/index.html">Publications</a></li>
<li><a href="/en/gov/statistics/index.html">Statistics &amp; data</a></li>
<li><a href="/en/newsite.html">About Canada.ca</a></li>
</ul>
</section>
<section class="col-sm-3 col-lg-3">
<h3>Transparency</h3>
<ul class="list-unstyled">
<li><a href="/en/transparency/reporting.html">Government-wide reporting</a></li>
<li><a href="/en/transparency/open.html">Open government</a></li>
<li><a href="http://www.tbs-sct.gc.ca/pd-dp/gr-rg/index-eng.asp" rel="external">Proactive disclosure</a></li>
<li><a href="/en/transparency/terms.html">Terms &amp; conditions</a></li>
<li><a href="/en/transparency/privacy.html">Privacy</a></li>
</ul>
</section>
<div class="col-sm-3 col-lg-3 brdr-lft">
<section>
<h3>News</h3>
<ul class="list-unstyled">
<li><a href="http://news.gc.ca/web/index-en.do" rel="external">Newsroom</a></li>
<li><a href="http://news.gc.ca/web/nwsprdct-en.do?mthd=tp&amp;crtr.tp1D=1" rel="external">News releases</a></li>
<li><a href="http://news.gc.ca/web/nwsprdct-en.do?mthd=tp&amp;crtr.tp1D=3" rel="external">Media advisories</a></li>
<li><a href="http://news.gc.ca/web/nwsprdct-en.do?mthd=tp&amp;crtr.tp1D=970" rel="external">Speeches</a></li>
<li><a href="http://news.gc.ca/web/nwsprdct-en.do?mthd=tp&amp;crtr.tp1D=980" rel="external">Statements</a></li>
</ul>
</section>
<section>
<h3>Contact information</h3>
<ul class="list-unstyled">
<li><a href="/en/contact/index.html#aat">Most asked-about topics</a></li>
<li><a href="/en/contact/index.html#spe">Specialized enquiries</a></li>
<li><a href="/en/contact/index.html#gen">General enquiries</a></li>
</ul>
</section>
</div>
<div class="col-sm-3 col-lg-3 brdr-lft">
<section>
<h3>Feedback</h3>
<p><a href="/en/contact/feedback.html"><img src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/feedback.png" class="img-responsive" alt="Feedback about this Web site"></a></p>
</section>
<section>
<h3>Social media</h3>
<p><a href="/en/social/index.html"><img src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/social.png" alt="Social media" class="img-responsive"></a></p>
</section>
<section>
<h3>Mobile centre</h3>
<p><a href="/en/mobile/index.html"><img src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/mobile.png" alt="Mobile centre" class="img-responsive"></a></p>
</section>
</div>
</div>

</nav>
<div class="brand"><div class="container"><div class="row "><div class="col-xs-6 visible-sm visible-xs tofpg"><a href="#wb-cont">Top of page <span class="glyphicon glyphicon-chevron-up"></span></a></div><div class="col-xs-6 col-md-12 text-right"><object type="image/svg+xml" tabindex="-1" role="img" data="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/wmms-blk.svg" aria-label="Symbol of the Government of Canada"></object></div></div></div></div>
</footer>

