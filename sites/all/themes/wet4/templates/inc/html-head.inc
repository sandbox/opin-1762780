<?php
 #@TODO: Remove in production
 error_reporting(E_ALL);
 ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="<?php echo $language->language; ?>"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo $language->language; ?>">
<!--<![endif]-->

<head>
  <meta charset="utf-8" />
  <!-- Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)
  wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html -->
  <link href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/favicon.ico" rel="shortcut icon" />
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <!-- Metatags from metatag module -->
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!--[if gte IE 9 | !IE ]><!-->
    <link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/wet-boew.min.css" />
  <!--<![endif]-->
  <link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/theme.min.css" />
  <link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/messages.min.css" />
  <!--[if lt IE 9]>
    <link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/messages-ie.min.css" />
    <link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/ie8-wet-boew.min.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
    <script src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/js/ie8-wet-boew.min.js"></script>
  <![endif]-->
  <!--[if lte IE 9]>
  <![endif]-->
  <script>dataLayer = []; dataLayer1 = [];</script>

  <noscript><link rel="stylesheet" href="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/css/noscript.min.css" /></noscript>
</head>
