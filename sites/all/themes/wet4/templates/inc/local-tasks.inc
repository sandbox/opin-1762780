<div class="container"><div class="row"><nav>
<?php if ($primary_local_tasks): ?>
  <h2 class="wb-inv">Primary tabs</h2>
  <ul class="nav nav-tabs">
    <?php print render($primary_local_tasks); ?>
  </ul>
<?php endif; ?>
<?php if ($secondary_local_tasks): ?>
  <h2 class="wb-inv">Secondary tabs</h2>
  <ul class="nav nav-tabs">
    <?php print render($secondary_local_tasks); ?>
  </ul>
<?php endif; ?>
</nav></div></div>
