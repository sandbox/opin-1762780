<div id="bg"><img src="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/img/splash/sp-bg-11.jpg" alt=""></div>
<main role="main">
<div class="sp-hb">
<div class="sp-bx col-xs-12">
<h1 class="wb-inv">Natural Resources Canada | <span lang="fr">Ressources naturelles Canada</span></h1>
<div class="row"><div class="col-xs-11 col-md-8"><object type="image/svg+xml" tabindex="-1" role="img" data="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/sig-spl.svg" aria-label="Government of Canada / Gouvernement du Canada"></object></div></div>
<div class="row">
<section class="col-xs-6 text-right">
<h2 class="wb-inv">The Source</h2>
<p><a href="/en/home" class="btn btn-primary">English</a></p>
</section>
<section class="col-xs-6" lang="fr">
<h2 class="wb-inv">La Source</h2>
<p><a href="/fr/accueil" class="btn btn-primary">Français</a></p>
</section>
</div>
</div>
<div class="sp-bx-bt col-xs-12">
<div class="row">
<div class="col-xs-7 col-md-8">
<a href="/terms-conditions" class="sp-lk">Terms &amp; conditions</a> <span class="glyphicon glyphicon-asterisk"></span> <a href="/fr/avis" class="sp-lk" lang="fr">Avis</a>
</div>
<div class="col-xs-5 col-md-4 text-right">
<object type="image/svg+xml" tabindex="-1" role="img" data="/<?php echo drupal_get_path('theme', 'wet4'); ?>/dist/assets/wmms-spl.svg" aria-label="Symbol of the Government of Canada / Symbole du gouvernement du Canada"></object>
<p></p>
</div>
</div>
</div>
</div>
</main>
