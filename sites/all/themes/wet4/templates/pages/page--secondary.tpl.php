<?php
 #@TODO: Remove in production
 error_reporting(E_ALL);
 ini_set("display_errors", 1);
?>

<?php include_once(drupal_get_path('theme', 'wet4') . '/templates/inc/page-head.inc'); ?>
<div class="container"><div class="row">
<main property="mainContentOfPage" class="col-md-9 col-md-push-3" role="main">
<header>
<h1 property="name" id="wb-cont" class="wb-inv"><?php print $site_name; ?></h1>

<div class="row">
<div class="col-md-12">
<h2 class="h1"><?php print $title; ?></h2>
<?php include_once(drupal_get_path('theme', 'wet4') . '/templates/inc/local-tasks.inc'); ?>
</div>
</div>
</header>
<div class="wb-eqht">
<?php print render($page['content']); ?>

</div>
<div class="row pagedetails"><div class="col-sm-5 col-xs-12 datemod"><dl id="wb-dtmd" property="dateModified"><dt>Date modified:&#32;</dt><dd><time>2014-07-04</time></dd></dl></div><div class="clear visible-xs"></div><div class="col-sm-4 col-md-5 col-xs-6"><a href="/en/contact/feedback.html" class="btn btn-default"><span class="glyphicon glyphicon-comment mrgn-rght-sm"></span>Feedback<span class="wb-inv"> about this web site</span></a></div><div class="col-sm-3 col-md-2 col-xs-6 text-right"><div class="wb-share" data-wb-share='{"lnkClass": "btn btn-default"}'></div></div><div class="clear visible-xs"></div></div>
</main>
<nav role="navigation" id="wb-sec" typeof="SiteNavigationElement" class="wb-sec col-md-3  col-md-pull-9">
  <?php print render($page['lhsm']); ?>
</nav>
</div></div>
<?php include_once(drupal_get_path('theme', 'wet4') . '/templates/inc/page-foot.inc'); ?>
