<?php
/**
* @file
* Displays the search form block.
*
* Available variables:
* - $search_form: The complete search form ready for print.
* - $search: Associative array of search elements. Can be used to print each
* form element separately.
*
* Default elements within $search:
* - $search['search_block_form']: Text input area wrapped in a div.
* - $search['actions']: Rendered form buttons.
* - $search['hidden']: Hidden form elements. Used to validate forms when
* submitted.
*
* Modules can add to the search form, so it is recommended to check for their
* existence before printing. The default keys will always exist. To check for
* a module-provided field, use code like this:
* @code
* <?php if (isset($search['extra_field'])): ?>
* <div class="extra-field">
* <?php print $search['extra_field']; ?>
* </div>
* <?php endif; ?>
* @endcode
*
* @see template_preprocess_search_block_form()
*/
?>
<?php # echo print_r($search['search_block_form'],true); ?>
<?php # echo print_r($search['hidden'],true); ?>
<?php # echo print_r($search['actions'],true); ?>
<section id="wb-srch" class="col-xs-6 text-right visible-md visible-lg">
<h2 class="wb-inv">Search</h2>
<form action="http://recherche-search.gc.ca/rGs/s_r?#wb-land" name="cse-search-box" method="get" role="search" class="form-inline">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript"> 
var autoCompletionOptions = { 'validLanguages' : 'en' };
google.load( 'search', '1');
google.setOnLoadCallback( function() {google.search.CustomSearchControl.attachAutoCompletionWithOptions( '008724028898028201144:knjjdikrhq0', document.getElementById( 'wb-srch-q' ), 'cse-search-box', autoCompletionOptions );});
google.setOnLoadCallback( function() {
	setTimeout( function(){
		google.search.CustomSearchControl.attachAutoCompletionWithOptions( '008724028898028201144:knjjdikrhq0', document.getElementById( 'wb-srch-q-imprt' ), 'cse-search-box', autoCompletionOptions );
	}, 2000);
});
</script>
<div class="form-group">
<label for="wb-srch-q" class="wb-inv">Search web site</label>
<input name="cdn" value="canada" type="hidden" />
<input name="st" value="s" type="hidden" />
<input name="num" value="10" type="hidden" />
<input name="langs" value="eng" type="hidden" />
<input name="st1rt" value="0" type="hidden">
<input name="s5bm3ts21rch" value="x" type="hidden" />
<input id="wb-srch-q" class="wb-srch-q form-control" name="q" type="search" 
       value="" 
       size="27" maxlength="150" 
       placeholder="Search Canada.ca" />
</div>
<div class="form-group submit">
<button type="submit" id="wb-srch-sub" class="btn btn-primary btn-small" name="wb-srch-sub"><span class="glyphicon-search glyphicon"></span><span class="wb-inv">Search</span></button>
</div>
</form>
</section>
