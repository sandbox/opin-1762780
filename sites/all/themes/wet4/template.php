<?php // $Id$

/**
 * @file template.php
 */

/**
 * Implementation of hook_preprocess()
 * 
 * This function checks to see if a hook has a preprocess file associated with 
 * it, and if so, loads it.
 * 
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */

function wet4_preprocess(&$vars, $hook) {
  global $user;
  $vars['is_admin'] = in_array('admin', $user->roles);
  $vars['logged_in'] = ($user->uid > 0) ? TRUE : FALSE;
  $vars['theme_path'] = base_path() . path_to_theme() .'/';

  // Include preprocess functions if and when required.
  if (is_file(drupal_get_path('theme', 'wet4') .'/templates/inc/template.preprocess-'. str_replace('_', '-', $hook) .'.inc')) {
    include(drupal_get_path('theme', 'wet4') .'/templates/inc/template.preprocess-'. str_replace('_', '-', $hook) .'.inc');
  }
}

function wet4_process(&$vars, $hook) {
  global $user;
  $vars['is_admin'] = in_array('admin', $user->roles);
  $vars['logged_in'] = ($user->uid > 0) ? TRUE : FALSE;
  $vars['theme_path'] = base_path() . path_to_theme() .'/';

  // Include preprocess functions if and when required.
  if (is_file(drupal_get_path('theme', 'wet4') .'/templates/inc/template.process-'. str_replace('_', '-', $hook) .'.inc')) {
    include(drupal_get_path('theme', 'wet4') .'/templates/inc/template.process-'. str_replace('_', '-', $hook) .'.inc');
  }
}

// Include custom functions.
include_once(drupal_get_path('theme', 'wet4') .'/templates/inc/template.custom-functions.inc');

// Include theme overrides.
include_once(drupal_get_path('theme', 'wet4') .'/templates/inc/template.theme-overrides.inc');

