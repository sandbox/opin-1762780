<?php

namespace DF;

class form_list {

  public static $forms_declaration = array();

  /** 
   * Get list of forms
   */
  public static function get_form_list() {
    return form_list::$forms_declaration;
  }
  
  /**
   * Get a form's default required marker label
   */
  public static function get_required_label($form_url) {
    if (isset(form_list::$forms_declaration[$form_url]['default_required_label'])) {
      return form_list::$forms_declaration[$form_url]['default_required_label'];
    }
    else {
      return '(required)';
    }
  }
  
  /**
   * Get a form by specifying a property value
   */
  public static function get_form($filter_property, $filter_value) {
    // If url is specified
    if (strtolower($filter_property) == 'url') {
      if (isset(form_list::$forms_declaration[$filter_value])) {
        return form_list::$forms_declaration[$filter_value];
      }
    }
    // If non-url property is specified
    else {
      foreach (form_list::$forms_declaration as $form) {
        if (isset($form[$filter_property]) AND $form[$filter_property] == $filter_value) {
          return $form;
        }
      }
      return NULL;
    }
  }
  
  /**
   * Get a form property value by specifying a value
   */
  public static function get_value_from($get_property, $filter_property, $filter_value) {
    // If url is specified
    if (strtolower($filter_property) == 'url') {
      if (isset(form_list::$forms_declaration[$filter_value][$get_property])) {
        return form_list::$forms_declaration[$filter_value][$get_property];
      }
    }
    foreach (form_list::$forms_declaration as $form) {
      if (isset($form[$filter_property]) AND $form[$filter_property] == $filter_value AND isset($form[$get_property])) {
        return $form[$get_property];
      }
    }
    return NULL;
  }
  
  /**
   * Get value from id (url)
   */
  public static function get_value($id_url, $property) {
    if (isset(form_list::$forms_declaration[$id_url][$property])) {
      return form_list::$forms_declaration[$id_url][$property];
    }
  }
}

