<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
                <p>A respondent.</p>
                <h2>Purpose</h2>
                <p>To respond to an application.</p>
                <h2>When should you file this form?</h2>
                <p>Within 15 business days after the date of the notice indicating that the application has been accepted, unless another time period is specified by the Agency.</p>
                <p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-2" target="_blank">section 19</a> of the Dispute Adjudication Rules for more information.</p>
                <h2>What happens next?</h2>
                <p>Upon receiving a copy of your answer, the applicant can file a reply to the answer.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
                  <p>Un défendeur.</p>
                  <h2>But</h2>
                  <p>Répondre à une demande.</p>
                  <h2>Quand devriez-vous déposer ce formulaire?</h2>
                  <p>Dans les quinze jours ouvrables suivant la date de l’avis d’acceptation de la demande, à moins que l’Office ne prescrive un autre délai.</p>
                  <p>Veuillez consulter l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-2\" target=\"_blank\">article 19</a> des Règles pour le règlement des différends pour de plus amples renseignements.&nbsp;</p>
                  <h2>Quelle est la prochaine étape?</h2>
                  <p>Une fois qu’il aura reçu une copie de votre réponse, le demandeur peut déposer une réplique à la réponse.</p>";


  $person_filing_form_options = array(
    '3' => t("I'm filing on behalf of an organization."),
    '4' => t("I'm a lawyer."),
    '1' => t("I'm filing on behalf of myself."),
    '2' => t("I'm filing on behalf of one or more respondents."),
    '5' => t("I'm a family member, friend, etc., who is representing the respondent(s).")
  );
  
  $person_filing_form_options_fr = array(
    '3' => "Je dépose le formulaire au nom d'un organisme.",
    '4' => "Je suis un avocat.",
    '1' => "Je dépose le formulaire en mon nom.",
    '2' => "Je dépose le formulaire au nom d'un ou de plusieurs défendeurs.",
    '5' => "Je suis un membre de la famille, un ami, etc., qui représente le ou les défendeurs.",
  );
  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Contact Information'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Coordonnées',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    // The fieldset for the first radio button select box
    'fieldset_person_filing_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Who is filing this form?'),
      // Translates the title attribute when in french
      '@L[fr]#title' => 'Qui dépose ce formulaire?',
      
      
      // The first radio button select box
      'person_filing_form' => array(
        //'#title' => '',
        //'@L[fr]#title' => '',
        '#type' => 'radios',
        '#options' => $person_filing_form_options,
        '@L[fr]#options' => $person_filing_form_options_fr,
        '#required' => TRUE,
        // Send the result of this field via ajax
        '@ajax_send' => TRUE,
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'build_action' => 'dynamic_forms_variable_set',
          'build_action_args' => array(
            array(
              'variable' => 'applicant_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'representative_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'organization_contact_copies',
              'value' => 1,
            ),
          ),
        ),
      ),
     ),
    
    
    
    'how_to_fill_org' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'If you are filing this answer on behalf of a company, an association or other organization, identify yourself and provide the contact information for the organization.',
        '@L[fr]#markup' => "Si vous déposez cette demande au nom d'une entreprise, d'une association ou d'un autre organisme, identifiez-vous et fournissez les coordonnées de l'organisme.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '3',
      ),
    ),
    
    'how_to_fill_multi' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Respondent" section below.<br>'
                      . 'Add the contact information for the other respondent(s) by clicking the "Add another respondent" button.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Défendeur » ci-dessous. <br>"
                          . "Ajoutez les coordonnées de tout autre défendeur en cliquant sur le bouton « Ajouter un autre défendeur ».",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '2',
      ),
    ),
    
    'how_to_fill_fam' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Representative" section.<br>'
                      . 'Add the contact information for whoever you\'re representing in the "Respondent" section below.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Représentant ».<br>"
                          . "Ajoutez les coordonnées de toute personne dont vous êtes le représentant dans la section « Défendeur » ci-dessous.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '5',
      ),
    ),
    
    'representatives' => array(
      '@extends' => 'representative_contact_group',
    ),
    
    'organizations' => array(
      '@extends' => 'organization_contact_group',
    ),
    
    'lawyer' => array(
      '@extends' => 'lawyer_contact_group',
    ),
    
    'applicants' => array(
      '@extends' => 'applicant_contact_group',
    ),
    
    'respondents' => array(
      '@extends' => 'respondent_contact_group',
      'contact_info' => array(
        '@extends' => 'contact_info_required',
        
        'consent' => array(
          '@extends' => 'description_text',
          '#markup' => "<p>We need the respondent's consent for you to act on their behalf. We will send you a pre-filled consent form automatically once you submit this form. The Agency will not be able to process the application until it receives the signed consent form.</p>",
          '@L[fr]#markup' => "<p>La déclaration du défendeur qui vous autorise à agir en son nom doit nous être fournie. Nous vous ferons automatiquement parvenir un formulaire de consentement prérempli, une fois que vous aurez soumis ce formulaire. L'Office ne sera pas en mesure de traiter la demande avant d'avoir reçu le formulaire de consentement signé.</p>",
          '@dependencies' => array(
            'value1' => '{person_filing_form}',
            'operator' => '==',
            'value2' => '5',
          ),
          '#weight' => -10,
        ),
        
        'same_as' => array(
          '@extends' => 'same_as',
          '#ajax' => array(
            'render_action' => 'dynamic_forms_same_as',
            'render_action_args' => array(
              'respondent_street_address#' => 'representative_street_address',
              'respondent_city#' => 'representative_city',
              'respondent_province#' => 'representative_province',
              'respondent_postal_code#' => 'representative_postal_code',
              'respondent_country#' => 'representative_country',
            ),
          ),
        ),
      ),
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'answer to application',
      '<form_short_name_fr>' => 'réponse à une demande',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs à la <form_short_name_fr>',
    ),
    
    'agree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you agree with in the application.'),
      '@L[fr]#title' => 'Indiquez les éléments dans la demande avec lesquels vous êtes en accord.',
      '#required' => TRUE,
    ),
    
    'disagree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you disagree with in the application.'),
      '@L[fr]#title' => 'Indiquez les éléments dans la demande avec lesquels vous êtes en désaccord.',
      '#required' => TRUE,
    ),
    
    'facts' => array(
      '#type' => 'textarea',
      '#title' => t('Provide a full description of the facts.'),
      '@L[fr]#title' => 'Donnez une description complète des faits.',
      '#required' => TRUE,
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the arguments in support of your answer.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre réponse.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your answer, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réponse, vous devez les déposer le même jour que votre réponse.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    )
  );
  
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    $person_filing = get_value('{person_filing_form}');

    if ($person_filing == 4 OR $person_filing == 1 OR $person_filing == 3) {        
      if (strpos($field, 'respondent_email') !== FALSE) {
        return FALSE;
      }
    }
  }
}


function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    // Get value of person filling form if available
    $person_filing = get_value('{person_filing_form}');
     
    // Set flag to create auth pdf if necessary
    if ($person_filing == 5) {
      set_system_variable('create_auth_pdf', TRUE);
    }
    else {
      set_system_variable('create_auth_pdf', FALSE);
    }
    
    if (strpos($field, 'respondent_email') !== FALSE) {
      if ($person_filing == '3') {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    
    if ($person_filing == '3') {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return TRUE;
      }
      elseif ($field == 'lawyer') {
        return FALSE;
      }
      elseif ($field == 'representatives') {
        return FALSE;
      }
      elseif ($field == 'respondents') {
        return FALSE;
      }
    }
    elseif ($person_filing == '4') {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return FALSE;
      }
      elseif ($field == 'lawyer') {
        return TRUE;
      }
      elseif ($field == 'representatives') {
        return FALSE;
      }
      elseif ($field == 'respondents') {
        return TRUE;
      }
      elseif ($field == 'respondent_add_button') {
        return FALSE;
      }
      elseif ($field == 'respondent_remove_button') {
        return FALSE;
      }
    }
    elseif ($person_filing == '1') {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return FALSE;
      }
      elseif ($field == 'lawyer') {
        return FALSE;
      }
      elseif ($field == 'representatives') {
        return FALSE;
      }
      elseif ($field == 'respondents') {
        return TRUE;
      }
      elseif ($field == 'respondent_add_button') {
        return FALSE;
      }
      elseif ($field == 'respondent_remove_button') {
        return FALSE;
      }
    }
    elseif ($person_filing == '2') {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return FALSE;
      }
      elseif ($field == 'lawyer') {
        return FALSE;
      }
      elseif ($field == 'representatives') {
        return FALSE;
      }
      elseif ($field == 'respondents') {
        return TRUE;
      }
    }
    elseif ($person_filing == '5') {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return FALSE;
      }
      elseif ($field == 'lawyer') {
        return FALSE;
      }
      elseif ($field == 'representatives') {
        return TRUE;
      }
      elseif ($field == 'respondents') {
        return TRUE;
      }
      elseif (strpos($field, 'respondent_same_as') !== FALSE) {
        return TRUE;
      }
    }
    else {
      if ($field == 'applicants') {
        return FALSE;
      }
      elseif ($field == 'organizations') {
        return FALSE;
      }
      elseif ($field == 'lawyer') {
        return FALSE;
      }
      elseif ($field == 'representatives') {
        return FALSE;
      }
      elseif ($field == 'respondents') {
        return FALSE;
      }
      elseif ($field == 'applicant_add_button') {
        return FALSE;
      }
      elseif ($field == 'applicant_remove_button') {
        return FALSE;
      }
    }     
  }
}

