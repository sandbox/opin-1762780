<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A party who is seeking disclosure of a document for which another person has made a request that it be considered confidential.</p>
<h2>Purpose</h2>
<p>To request disclosure of a document for which a person is claiming confidentiality.</p>
<h2>When should you file this form?</h2>
<p>Within 5 business days after the day on which you receive a copy of the request for confidentiality.</p>
<h2>What happens next?</h2>
<p>The person making the request for confidentiality may oppose your request for disclosure in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a>&nbsp;within 3 business days after the day on which they receive a copy of your request for disclosure.</p>
<p>The Agency will consider whether to grant the request after reviewing all the submissions of the parties.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a> of the Dispute Adjudication Rules for more information.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une partie qui demande la communication d’un document ayant fait l’objet d’une requête de confidentialité par une autre personne.&nbsp;</p>
<h2>But</h2>
<p>Demander la communication d’un document qui fait l’objet d’une requête de confidentialité.&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les cinq jours ouvrables suivant la date de réception de la copie de la requête de confidentialité.&nbsp;</p>
<h2>Quelle est la prochaine étape?</h2>
<p>La personne qui dépose la requête de confidentialité peut s’opposer à la requête de communication en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a>&nbsp;dans les trois jours ouvrables suivant la date de réception de la copie de la requête de communication. <strong></strong></p>
<p>Après avoir examiné toutes les présentations des parties, l’Office déterminera si la requête est accordée ou non.</p>
<p>Veuillez consulter l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a> des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'request for disclosure',
      '<form_short_name_fr>' => 'requête de divulgation',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs à la <form_short_name_fr>',
    ),
    
    'ident_docs' => array(
      '#type' => 'textarea',
      '#title' => t('Identify the document(s) for which you request disclosure.'),
      '@L[fr]#title' => 'Indiquez les documents pour lesquels vous demandez la divulgation.',
      '#required' => TRUE,
    ),
    
    'ident_individuals' => array(
      '#type' => 'textarea',
      '#title' => t('Identify the individuals who require access to the document(s).'),
      '@L[fr]#title' => "Indiquez les personnes pour lesquelles l'accès aux documents est demandé.",
      '#required' => TRUE,
    ),
    
    'explain_relevance_docs' => array(
      '#type' => 'textarea',
      '#title' => t('Explain the relevance of the document(s) being requested and the public interest in its/their disclosure.'),
      '@L[fr]#title' => "Expliquez la pertinence des documents demandés et l'intérêt du public.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your request, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre requête, vous devez les déposer le même jour que votre requête.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

