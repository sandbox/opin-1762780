<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>An applicant or a respondent adverse in interest to the intervener in the dispute proceeding.</p>
<h2>Purpose</h2>
<p>To respond to an intervention submitted by an intervener.</p>
<h2>When should you file this form?</h2>
<p>Within 5 business days after the day on which you receive a copy of the intervention, unless another time period is specified by the Agency.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-5" target="_blank">section 22</a> of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>The response will be placed on the Agency’s record.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>
</div>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Un demandeur ou un défendeur qui a des intérêts opposés à ceux d’un intervenant dans le cadre d’une instance de règlement d’un différend.&nbsp;</p>
<h2>But</h2>
<p>Répondre à une intervention présentée par un intervenant.&nbsp;<strong></strong></p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les cinq jours ouvrables suivant la date de réception de la copie de l’intervention, à moins que l’Office ne prescrive un autre délai. <strong></strong></p>
<p>Veuillez consulter l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-5\" target=\"_blank\">article 22</a> des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>La réponse sera versée aux archives publiques de l’Office.&nbsp;</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'response to intervention',
      '<form_short_name_fr>' => 'réponse à l’intervention',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs à la <form_short_name_fr>',
    ),
    
    'agree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you agree with in the intervention.'),
      '@L[fr]#title' => "Indiquez les éléments dans l'intervention avec lesquels vous êtes en accord.",
      '#required' => TRUE,
    ),
    
    'disagree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you disagree with in the intervention.'),
      '@L[fr]#title' => "Indiquez les éléments dans l'intervention avec lesquels vous êtes en désaccord.",
      '#required' => TRUE,
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the arguments in support of your response.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre réponse.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your response, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réponse, vous devez les déposer le même jour que votre réponse.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

