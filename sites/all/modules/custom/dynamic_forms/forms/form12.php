<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>Any party to whom a question is directed or from whom a document is requested under <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-7" target="_blank">subsection 24(1)</a> of the Rules.</p>
<h2>Purpose</h2>
<p>If questions or a document request have been directed to you, you must provide a complete response to each question or a copy of any document requested</p>
<p>If you object to a question or to producing a document, you must file an objection as part of your response.</p>
<p>If any part of your response contains information that you believe is confidential, you can file a request for confidentiality in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a>&nbsp;(<a href="https://services.cta-otc.gc.ca/forms" target="_blank">Form 17</a>).</p>
<h2>When should you file this form?</h2>
<p>Within 5 business days after the day on which you receive a copy of the notice to respond to written questions or to produce documents.</p>
<h2>What happens next?</h2>
<p>If the person who directed questions or the document request to you is not satisfied with your response or opposes your objection, they may file a request for the Agency to require you to provide a complete response.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-7" target="_blank">sections 24</a> and <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-6">32</a> of the Dispute Adjudication Rules for more information.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Toute partie à qui une question est posée ou à qui on demande de produire des documents en vertu du <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-7\" target=\"_blank\">paragraphe 24(1)</a> des Règles.</p>
<h2>But</h2>
<p>Si on vous a posé des questions ou demandé de produire des documents, vous devez déposer une réponse complète à chacune des questions ou produire une copie de tous les documents demandés. &nbsp;</p>
<p>Si vous vous opposez à une question ou à la production de documents, vous devez déposer une opposition dans le cadre de votre réponse.</p>
<p>Si une quelconque partie de votre réponse contient des renseignements que vous jugez confidentiels, vous pouvez déposer une requête de confidentialité en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a>&nbsp;(<a href=\"https://services.cta-otc.gc.ca/fra/formulaires\" target=\"_blank\">formulaire 17</a>).&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les cinq jours ouvrables suivant la date de réception de la copie de l’avis visant la réponse aux questions écrites ou la production de documents.&nbsp;</p>
<h2>Quelle est la prochaine étape?</h2>
<p>Si la personne qui vous a posé des questions ou demandé de produire des documents n'est pas satisfaite de votre réponse ou conteste votre opposition, elle peut déposer une requête pour que l’Office vous oblige à fournir une réponse complète à l'avis. &nbsp;<strong></strong></p>
<p>Veuillez consulter les <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-7\" target=\"_blank\">articles 24</a> et <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-6\" target=\"_blank\">32</a> des Règles pour le règlement des différends pour de plus amples renseignements.&nbsp;</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'response to written questions or request for documents',
      '<form_short_name_fr>' => 'réponses aux questions écrites ou à la demande de documents',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs aux <form_short_name_fr>',
    ),
    
    'fieldset_responding' => array(
      '#type' => 'fieldset',
      '#title' => t('I am responding by:'),
      '@L[fr]#title' => "Je réponds en :",
      
      'responding' => array(
        '#required' => TRUE,
        '#type' => 'radios',
        '@ajax_send' => TRUE,
        //'@required_label' => '',
        '#options' => array(
          'questions' => t('Providing answers to written questions'),
          'documents' => t('Providing documents'),
          'objections' => t('Filing an objection'),
        ),
        '@L[fr]#options' => array(
          'questions' => 'Formulant des réponses aux questions écrites.',
          'documents' => 'Fournissant des documents.',
          'objections' => 'Déposant une opposition.',
        ),
      ),
    ),
    
    
    'questions' => array(
      '#type' => 'fieldset',
      '#title' => 'Response to written questions',
      '@L[fr]#title' => 'Réponse aux questions écrites',
      
      '@dependencies' => array(
        'value1' => '{responding}',
        'operator' => '==',
        'value2' => 'questions',
      ),
      
      'response_questions' => array(
        '#type' => 'textarea',
        '#title' => t('Provide your answers to the questions'),
        '@L[fr]#title' => "Formulez vos réponses aux questions.",
        '#required' => TRUE,
      ),
    ),
    
    'documents' => array(
      '#type' => 'fieldset',
      '#title' => 'Response to a request for documents',
      '@L[fr]#title' => 'Réponse à une demande de documents',
      
      '@dependencies' => array(
        'value1' => '{responding}',
        'operator' => '==',
        'value2' => 'documents',
      ),
      
      'response_documents' => array(
        '#type' => 'textarea',
        '#title' => t('List the documents that you will provide'),
        '@L[fr]#title' => "Dressez une liste des documents produits.",
        '#required' => TRUE,
      ),
      
      'how_do_i_file_my_docs' => array(
        '@extends' => 'question_text2',
        'heading' => array(
          '#markup' => 'How do I file my documents?',
          '@L[fr]#markup' => "Comment dois-je déposer mes documents?",
        ),
        'body' => array(
          '#markup' => 'After you submit the form, you will be emailed a link to a secure file transfer system. You will have an account to manage your documents.</p>'
          . '<p>Please upload your files right away.</p>'
          . '<p>You can also file documents by fax, courier, or personal delivery.',
          '@L[fr]#markup' => "Une fois que vous aurez déposé le formulaire, vous recevrez par courriel un lien vers un système sécurisé de transfert de fichiers. Vous aurez un compte qui vous permettra de gérer vos documents.</p>"
          . "<p>Veuillez télécharger vos fichiers immédiatement.</p>"
          . "<p>Vous avez aussi la possibilité de déposer des documents par télécopieur, par service de messagerie, ou en main propre.",
        ),
      ),
    ),
    
    
    
    'objections' => array(
      '#type' => 'fieldset',
      '#title' => 'Objection',
      '@L[fr]#title' => 'Opposition',
      
      '@dependencies' => array(
        'value1' => '{responding}',
        'operator' => '==',
        'value2' => 'objections',
      ),
      
      'reasons_objection' => array(
        '#type' => 'textarea',
        '#title' => t('Provide a clear and concise explanation of the reasons underlying your objection including, as applicable, the relevance of the information or document and its availability for production'),
        '@L[fr]#title' => "Donnez un exposé clair et concis des motifs de votre opposition, y compris, le cas échéant, la pertinence des renseignements ou du document ainsi que leur disponibilité aux fins de production.",
        '#required' => TRUE,
      ),
      'list_documents' => array(
        '#type' => 'textarea',
        '#title' => t('List any document that is relevant in explaining or supporting your objection'),
        '@L[fr]#title' => "Dressez la liste des documents pertinents à l'appui de votre opposition.",
      ),
      'other_information' => array(
        '#type' => 'textarea',
        '#title' => t('Provide any other information or list any document that is in your possession or control and that would be of assistance to the party who asked the question or asked for the production of documents'),
        '@L[fr]#title' => "Fournissez tout autre renseignement ou document en votre possession ou sous votre garde qui est susceptible d'aider la partie qui a posé les questions ou présenté la requête de production de document.",
      ),
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your response or objection(s), you must file them on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réponse ou de votre opposition, vous devez les déposer le même jour que votre réponse ou votre opposition.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

