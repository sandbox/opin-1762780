<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person who has a <em>substantial and direct interest</em> in a dispute proceeding may file a request to intervene.</p>
<h2>Purpose</h2>
<p>To obtain authorization from the Agency to intervene so that you can make your views known and/or otherwise participate in the dispute proceeding.</p>
<p>If your interest is not "substantial and direct", you may file a position statement instead in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-6" target="_blank">section 23</a> (<a href="https://services.cta-otc.gc.ca/forms" target="_blank">Form 10</a>). Neither an intervener nor a person who files a position statement is considered to be a party to the proceeding and thus will have no rights to participate or to receive further correspondence concerning the file.</p>
<p>However, a person who is requesting authorization to intervene, as part of the participation rights that they are requesting, may also ask for party status in the proceeding.</p>
<h2>When should you file this form?</h2>
<p>Within 10 business days after the day on which you become aware of the application or before the close of pleadings whichever is earlier.</p>
<h2>What happens next?</h2>
<p>The Agency will review your request and determine if you will be granted authorization to intervene in the dispute proceeding.</p>
<p>The Agency will also inform you as to the extent of your participation rights in the dispute proceeding.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-3" target="_blank">section 29</a> of the Dispute Adjudication Rules for more information.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne qui a un intérêt direct et substantiel dans une instance de règlement d’un différend peut déposer une requête pour demander l’autorisation d’intervenir.</p>
<h2>But</h2>
<p>Obtenir de l’Office l’autorisation d’intervenir afin de faire connaître vos opinions ou de participer à l’instance. <strong></strong></p>
<p>Si votre intérêt n’est pas « direct et substantiel », vous pouvez déposer un énoncé de position en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-6\" target=\"_blank\">article 23</a> (<a href=\"https://services.cta-otc.gc.ca/fra/formulaires\" target=\"_blank\">formulaire 10</a>). Un intervenant ou une personne qui dépose un énoncé de position n’est pas considéré comme une partie à l’instance et n’a par conséquent aucun droit d’y participer ni de recevoir des avis concernant le dossier.</p>
<p>Toutefois, une personne qui demande l’autorisation d’intervenir peut également, dans le cadre des droits de participation qu'elle demande, demander d’obtenir le statut de partie à l’instance.&nbsp;<span style=\"text-decoration: underline;\"></span></p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les dix jours ouvrables suivant la date à laquelle vous avez pris connaissance de la demande et, en tout état de cause, avant la clôture des actes de procédure.&nbsp;</p>
<h2>Quelle est la prochaine étape?</h2>
<p>L’Office examinera votre requête et déterminera s’il vous accorde ou non l’autorisation d’intervenir dans l’instance de règlement du différend. <strong></strong></p>
<p>L’Office vous indiquera également l’étendue de vos droits de participation à l’instance.</p>
<p>Veuillez consulter l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-3\" target=\"_blank\">article 29</a> des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<p></p>";


  $person_filing_form_options = array(
    '1' => t("I'm filing on behalf of myself"),
    '2' => t("I'm filing on behalf myself and other individuals"),
    '3' => t("I'm filing on behalf one or more organizations"),
    '4' => t("I'm a lawyer"),
    '5' => t("I'm a family member, friend, etc., who is representing the person requesting to intervene.")
  );
  
  $person_filing_form_options_fr = array(
    '1' => "Je dépose le formulaire en mon nom.",
    '2' => "Je dépose le formulaire en mon nom et au nom d'autres personnes.",
    '3' => "Je dépose le formulaire au nom d'un ou de plusieurs organismes.",
    '4' => "Je suis un avocat.",
    '5' => "Je suis un membre de la famille, un ami, etc., qui représente la personne ayant déposé la requête pour intervention.",
  );
  
  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Contact Information'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Coordonnées',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    // The fieldset for the first radio button select box
    'fieldset_person_filing_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Who is filing this form?'),
      // Translates the title attribute when in french
      '@L[fr]#title' => 'Qui dépose ce formulaire?',
      
      
      // The first radio button select box
      'person_filing_form' => array(
        //'#title' => '',
        //'@L[fr]#title' => '',
        '#type' => 'radios',
        '#options' => $person_filing_form_options,
        '@L[fr]#options' => $person_filing_form_options_fr,
        '#required' => TRUE,
        // Send the result of this field via ajax
        '@ajax_send' => TRUE,
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'build_action' => 'dynamic_forms_variable_set',
          'build_action_args' => array(
            array(
              'variable' => 'applicant_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'representative_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'organization_contact_copies',
              'value' => 1,
            ),
          ),
        ),
      ),
     ),
    
    
    
    'how_to_fill_multi' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Person requesting to intervene" section below.'
                     .'<br>Add the contact information for the other people by clicking the "Add another person" button.',
        '@L[fr]#markup' => 'Fournissez vos coordonnées dans la section « Personne qui dépose la requête pour intervention » ci-dessous. Ajoutez les coordonnées de toute autre personne en cliquant sur le bouton « Ajouter une autre personne ».',
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '2',
      ),
    ),
    
    'how_to_fill_org' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'If you are filing this request on behalf of a company, an association or other organization, identify yourself and provide the contact information for the organization.',
        '@L[fr]#markup' => "Si vous déposez cette requête au nom d'une entreprise, d'une association ou d'un autre organisme, identifiez-vous et fournissez les coordonnées de l'organisme.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '3',
      ),
    ),
    
    'how_to_fill_fam' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Representative" section.'
                     .'<br>Add the contact information for whoever you\'re representing in the "Person requesting to intervene" section below.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Représentant » ci-dessous."
                          . "<br>Ajoutez les coordonnées de toute personne dont vous êtes le représentant dans la section « Personne qui dépose la requête pour intervention ».",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '5',
      ),
    ),
    
    'representatives' => array(
      '@extends' => 'representative_contact_group',
    ),
    
    'organizations' => array(
      '@extends' => 'organization_contact_group',
    ),
    
    'lawyer' => array(
      '@extends' => 'lawyer_contact_group',
    ),
    
    'applicants' => array(
      'contact_info' => array(
        '@extends' => 'contact_info_required',
        '#type' => 'fieldset',
        '@#title' => array(
          'condition' => array(
            'value1' => '<B:applicant_contact_copies>',
            'operator' => '==',
            'value2' => 1,
            'eval_time' => 'B',
          ),
          'source_true' => t('Contact information: Person requesting to intervene'),
          'source_false' => t('Contact information: Person requesting to intervene') . '[B: applicant_contact_counter]',
        ),
        '@L[fr]@#title' => array(
          'condition' => array(
            'value1' => '<B:applicant_contact_copies>',
            'operator' => '==',
            'value2' => 1,
            'eval_time' => 'B',
          ),
          'source_true' => 'Coordonnées : personne ayant déposé la requête pour intervention',
          'source_false' => 'Coordonnées : personne ayant déposé la requête pour intervention' . '[B: applicant_contact_counter]',
        ),
        
        'consent' => array(
          '@extends' => 'description_text',
          '#markup' => "<p>We need the person's consent for you to act on their behalf. We will send you a pre-filled consent form automatically once you submit this form. The Agency will not be able to process the application until it receives the signed consent form.</p>",
          '@L[fr]#markup' => "<p>Nous vous ferons automatiquement parvenir un formulaire de consentement prérempli, une fois que vous aurez soumis ce formulaire. L'Office ne sera pas en mesure de traiter cette demande avant d'avoir reçu le formulaire de consentement signé.</p>",
          '@dependencies' => array(
            'value1' => '{person_filing_form}',
            'operator' => '==',
            'value2' => '5',
          ),
          '#weight' => -10,
        ),
        
        'same_as' => array(
          '#title' => t('Same address as representative'),
          '@L[fr]#title' => 'Même adresse que celle du représentant',
          '#type' => 'checkbox',
          '@ajax_send' => TRUE,
          '#ajax' => array(
            'render_action' => 'dynamic_forms_same_as',
            'render_action_args' => array(
              'applicant_street_address#' => 'representative_street_address',
              'applicant_city#' => 'representative_city',
              'applicant_province#' => 'representative_province',
              'applicant_postal_code#' => 'representative_postal_code',
              'applicant_country#' => 'representative_country',
            ),
          ),
        ),
      ),
      '@extends' => 'applicant_contact_group',
            
      'remove_button' => array(
        '#value' => t('-Remove person'),
        '@L[fr]#value' => '-Supprimer une personne',
      ),
      'add_button' => array(
        '#value' => t('+Add another person'),
        '@L[fr]#value' => '+Ajouter une autre personne',
      ),
      
    ),
    
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'request to intervene',
      '<form_short_name_fr>' => 'requête pour intervention',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs à la <form_short_name_fr>',
    ),
    
    'fieldset_support_position' => array(
      '#type' => 'fieldset',
      '#title' => t('I support the position of:'),
      '@L[fr]#title' => 'J\'appuie la position :',
      
      'support_position' => array(
        '#title' => '',
        '@L[fr]#title' => '',
        '#required' => TRUE,
        '#type' => 'radios',
        //'@required_label' => '',
        '#options' => array(
          'appl' => t('The applicant'),
          'resp' => t('The respondent'),
          'neit' => t('Neither the applicant nor the respondent'),
        ),
        '@L[fr]#options' => array(
          'appl' => 'Du demandeur',
          'resp' => 'Du défendeur',
          'neit' => 'Je n\'appuie ni la position du demandeur ni celle du défendeur',
        ),
      ),
    ),
    
    'reasons' => array(
      '#type' => 'textarea',
      '#title' => t('Demonstrate the reasons why you have a substantial and direct interest in the dispute proceeding.'),
      '@L[fr]#title' => "Faites la démonstration de votre intérêt direct et substantiel dans l'instance de règlement des différends.",
      '#required' => TRUE,
    ),
    
    'date_aware' => array(
      '#type' => 'textarea',
      '#title' => t('Specify the date on which you became aware of the application.'),
      '@L[fr]#title' => 'Indiquez la date à laquelle vous avez pris connaissance de la demande.',
      '#required' => TRUE,
    ),
    
    'set_participation_rights' => array(
      '#type' => 'textarea',
      '#title' => t('Set out the participation rights that you wish to be granted in the dispute proceeding (i.e. responding to pleadings of other parties, submitting questions to other parties). '),
      '@L[fr]#title' => "Énoncez les droits de participation que vous souhaitez vous voir accorder dans l'instance (nommément, le droit de répondre aux arguments des autres parties, de soumettre des questions aux autres parties).",
      '#required' => TRUE,
    ),
        
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your request, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre requête, vous devez les déposer le même jour que votre requête.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    'submission_group' => array(
      '@extends' => 'submission_page',
    ),
    'personal_info' => array(
      '@extends' => 'personal_info',
    )
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    )
  );
  
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}


/**
 * Check requirements
 */
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    $field_value = get_value('{person_filing_form}');

    if ($field_value == 4 OR $field_value == 5) {
        
      /*
      $contact_elements = array(
        'applicant_first_name',
        'applicant_last_name',
        'applicant_street_address',
        'applicant_city',
        'applicant_province',
        'applicant_postal_code',
        'applicant_country',
        'applicant_email',
        'applicant_phone_number',
      );

      foreach ($contact_elements as $contact) {
        if (strpos($field, $contact) !== FALSE) {
          return TRUE;
        }
      }
       */
      if (strpos($field, 'applicant_email') !== FALSE) {
        return FALSE;
      }
    }
  }
}


function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    // Get value of person filling form if available
    $field_value = get_value('{person_filing_form}');
    
    // Set flag to create auth pdf if necessary
    if ($field_value == 5) {
      set_system_variable('create_auth_pdf', TRUE);
    }
    else {
      set_system_variable('create_auth_pdf', FALSE);
    }
    
    if ($field == 'applicants') {
      // Evaluate result
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value != 3);
      }
    }   
    elseif ($field == 'organizations') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 3);
      }
    }
    elseif ($field == 'lawyer') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 4);
      }
    }
    elseif ($field == 'representatives') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 5);
      }
    }
    elseif ($field == 'respondents') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }    
    elseif ($field == 'applicant_add_button') {
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2);
      }
      else {
        return FALSE;
      }
    }
    elseif ($field == 'applicant_remove_button') {
      if (isset($result) AND $result === FALSE) {
        return FALSE;
      }
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2);
      }
      else {
        return FALSE;
      }
    }
    elseif (strpos($field, 'applicant_organization') !== FALSE) {
      if ($field_value == 4 OR $field_value == 5) {
        return TRUE;
      }
    }
    elseif ($field == 'applicant_same_as1') {
      if (isset($field_value)) {
        return ($field_value == 5);
      }
    }
  }
}

