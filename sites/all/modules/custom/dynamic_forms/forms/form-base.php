<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  
  // Declare important PHP variables used by array
  $intro_text = '';

  $intro_text_fr = "";


  $person_filing_form_options = array(
    '1' => t("I'm filing on behalf of myself"),
    '2' => t("I'm filing on behalf myself and other individuals"),
    '3' => t("I'm filing on behalf one or more organizations"),
    '4' => t("I'm a lawyer"),
    '5' => t("I'm a family member, friends, etc, who is representing the applicant(s)")
  );
  
  $person_filing_form_options_fr = array(
    '1' => "Je dépose le formulaire en mon nom.",
    '2' => "Je dépose le formulaire en mon nom et au nom d'autres personnes.",
    '3' => "Je dépose le formulaire au nom d'un ou de plusieurs organismes.",
    '4' => "Je suis un avocat.",
    '5' => "Je suis un membre de la famille, un ami, etc., qui représente le ou les demandeurs.",
  );
  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Page 1 of 2: Contact Information'), 
      '@L[fr]#markup' => 'Partie 1 de 2 : Coordonnées',
    ),
    
    // The fieldset for the first radio button select box
    'fieldset_person_filing_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Who is filing this form?'),
      // Translates the title attribute when in french
      '@L[fr]#title' => 'Qui dépose ce formulaire?',
      
      
      // The first radio button select box
      'person_filing_form' => array(
        //'#title' => '',
        //'@L[fr]#title' => '',
        '#type' => 'radios',
        '#options' => $person_filing_form_options,
        '@L[fr]#options' => $person_filing_form_options_fr,
        '#required' => TRUE,
        // Send the result of this field via ajax
        '@ajax_send' => TRUE,
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'build_action' => 'dynamic_forms_variable_set',
          'build_action_args' => array(
            array(
              'variable' => 'applicant_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'representative_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'organization_contact_copies',
              'value' => 1,
            ),
          ),
        ),
      ),
     ),
    
    
    'how_to_fill_multi' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Applicant" section below.
                     <br>Add the contact information for the other applicant(s) by clicking the "Add another applicant" button.',
        '@L[fr]#markup' => 'Fournissez vos coordonnées dans la section « Demandeur » ci-dessous. Ajoutez les coordonnées de tout autre demandeur en cliquant sur le bouton « Ajouter un autre demandeur ».',
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '2',
      ),
    ),
    
    
    'how_to_fill_org' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'If you are filing this application on behalf of a company, an association or other organization, identify yourself and provide the contact information for the organization.',
        '@L[fr]#markup' => "Si vous déposez cette demande au nom d'une entreprise, d'une association ou d'un autre organisme, identifiez-vous et fournissez les coordonnées de l'organisme.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '3',
      ),
    ),
    
    'how_to_fill_fam' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Representative" section.
                    <br>Add the contact information for whoever you\'re representing in the "Applicant" section below.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Représentant ». Ajoutez les coordonnées de toute personne dont vous êtes le représentant dans la section « Demandeur » ci-dessous.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '5',
      ),
    ),
    
    'representatives' => array(
      '@extends' => 'representative_contact_group',
    ),
    
    'organizations' => array(
      '@extends' => 'organization_contact_group',
    ),
    
    'lawyer' => array(
      '@extends' => 'lawyer_contact_group',
    ),
    
    'applicants' => array(
      'contact_info' => array(
        'consent' => array(
          '@extends' => 'description_text',
          '#markup' => "<p>We need the applicant's consent for you to act on their behalf. We will send you a pre-filled consent form automatically, once you submit this form. The Agency will not be able to process the application until it receives the signed consent form.</p>",
          '@L[fr]#markup' => "<p>La déclaration du demandeur qui vous autorise à agir en son nom doit nous être fournie. Nous vous ferons automatiquement parvenir un formulaire de consentement prérempli, une fois que vous aurez soumis ce formulaire. L'Office ne sera pas en mesure de traiter la demande avant d'avoir reçu le formulaire de consentement signé.</p>",
          '@dependencies' => array(
            'value1' => '{person_filing_form}',
            'operator' => '==',
            'value2' => '5',
          ),
          '#weight' => -10,
        ),
      ),
      '@extends' => 'applicant_contact_group',
    ),
    
    'respondants' => array(
      '@extends' => 'respondant_contact_group',
    ),
    
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'fieldset',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'application',
      '<form_short_name_fr>' => 'demande',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '#markup' => t('Part 2 of 2: Details of the Application'),
      '@L[fr]#markup' => 'Partie 2 de 2 : Détails relatifs à la demande',
    ),
    
    'full_description' => array(
      '#type' => 'textarea',
      '#title' => t('Provide a full description of the facts.'),
      '@L[fr]#title' => 'Donnez une description complète des faits.',
      '#required' => TRUE,
    ),
    
    'state_issues' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly state the issues.'),
      '@L[fr]#title' => 'Énoncez clairement les questions en litige.',
      '#required' => TRUE,
    ),
    
    'legislative_provisions' => array(
      '#type' => 'textarea',
      '#title' => t('Identify any legislative provisions on which you are relying.'),
      '@L[fr]#title' => 'Indiquez toutes les dispositions législatives sur lesquelles vous fondez votre demande.',
      '#required' => TRUE,
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the arguments in support of your application.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre demande.",
      '#required' => TRUE,
    ),
    
    'relief_seeking' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the relief you are seeking.'),
      '@L[fr]#title' => 'Indiquez clairement la réparation demandée.',
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your application, you must file them on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre demande, vous devez les déposer le même jour que votre demande.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
    'personal_info' => array(
      '@extends' => 'personal_info',
    )
  );
    
  $formArray = array(
    //'@overrides' => array(
      //'person_filing_form->#title' => 'ITS OVERRIDDEN!! What, what!'
      //'@L[fr]#title' => '',
    //)
  );
  
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;

  return $formArray;
}

/**
 * Check requirements
 */
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    $field_value = get_value('{person_filing_form}');

    if ($field_value == 4 OR $field_value == 5) {
        
      /*
      $contact_elements = array(
        'applicant_first_name',
        'applicant_last_name',
        'applicant_street_address',
        'applicant_city',
        'applicant_province',
        'applicant_postal_code',
        'applicant_country',
        'applicant_email',
        'applicant_phone_number',
      );

      foreach ($contact_elements as $contact) {
        if (strpos($field, $contact) !== FALSE) {
          return TRUE;
        }
      }
       */
      if (strpos($field, 'applicant_email') !== FALSE) {
        return FALSE;
      }
    }
  }
}


function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    // Get value of person filling form if available
    $field_value = get_value('{person_filing_form}');
      
    if ($field == 'applicants') {
      // Evaluate result
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value != 3);
      }
    }   
    elseif ($field == 'organizations') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 3);
      }
    }
    elseif ($field == 'lawyer') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 4);
      }
    }
    elseif ($field == 'representatives') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 5);
      }
    }
    elseif ($field == 'respondants') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }    
    elseif ($field == 'applicant_add_button') {
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2 OR $field_value == 4 OR $field_value == 5);
      }
      else {
        return FALSE;
      }
    }
    elseif ($field == 'applicant_remove_button') {
      if (isset($result) AND $result === FALSE) {
        return FALSE;
      }
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2 OR $field_value == 4 OR $field_value == 5);
      }
      else {
        return FALSE;
      }
    }
    elseif (strpos($field, 'applicant_organization') !== FALSE) {
      if ($field_value == 4 OR $field_value == 5) {
        return TRUE;
      }
    }
  }
}

