<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person who has made a request to intervene that has been granted by the Agency.</p>
<h2>Purpose</h2>
<p>To file a written statement with the Agency, once the request to intervene has been granted by the Agency under <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-3" target="_blank">section 29</a>.</p>
<h2>When should you file this form?</h2>
<p>Within 5 business days after the day on which your request to intervene is granted by the Agency, unless another time period has been specified by the Agency.</p>
<p>Refer to the <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-4" target="_blank">sections 21</a> and <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-3" target="_blank">29</a> of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>Your intervention will be placed on the Agency’s public record unless a request for confidentiality is made in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a>&nbsp;and accepted by the Agency.</p>
<p>An applicant or a respondent who is adverse in interest may respond to your intervention within 5 business days after the day on which they receive a copy of the intervention, unless another time period is specified by the Agency.</p>
<p>You may be required to respond to questions or information requests from the Agency or from any other party to the proceeding that does not agree with issues raised in the intervention.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>
</div>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne qui a présenté une requête d’intervention qui a été accordée par l’Office.</p>
<h2>But</h2>
<p>Déposer une déclaration écrite auprès de l’Office une fois que la requête d’intervention a été accordée par ce dernier en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-3\" target=\"_blank\">article 29</a>.&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les cinq jours ouvrables suivant la date à laquelle la requête d’intervention a été accordée par l’Office, à moins que ce dernier ne prescrive un autre délai.</p>
<p>Veuillez consulter les <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-4\" target=\"_blank\">articles 21</a> et <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-3\" target=\"_blank\">29</a> des Règles pour le règlement des différends pour de plus amples renseignements.&nbsp;</p>
<h2>Quelle est la prochaine étape?</h2>
<p>Votre intervention sera versée aux archives publiques de l’Office à moins qu’une requête de confidentialité soit présentée en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a> et acceptée par l’Office.</p>
<p>Un demandeur ou un défendeur qui a des intérêts opposés à ceux d’un intervenant et qui souhaite déposer une réponse à l’intervention le fait dans les cinq jours ouvrables suivant la date de réception de la copie de l’intervention, à moins que l’Office ne prescrive un autre délai.&nbsp;</p>
<p>Vous pourriez devoir répondre à des questions ou à des demandes de renseignements de l’Office ou de toute autre partie à l'instance qui n’est pas d’accord avec les questions soulevées dans l’intervention.</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'intervention',
      '<form_short_name_fr>' => 'intervention',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => "Partie 2 de 3 : Détails relatifs à l'<form_short_name_fr>",
    ),
    
    'fieldset_support_position' => array(
      '#type' => 'fieldset',
      '#title' => t('I support the position of:'),
      '@L[fr]#title' => 'J\'appuie la position :',
      
      'support_position' => array(
        '#required' => TRUE,
        '#type' => 'radios',
        //'@required_label' => '',
        '#options' => array(
          'appl' => t('The applicant'),
          'resp' => t('The respondent'),
          'neit' => t('Neither the applicant nor the respondent'),
        ),
        '@L[fr]#options' => array(
          'appl' => 'Du demandeur',
          'resp' => 'Du défendeur',
          'neit' => 'Je n\'appuie ni la position du demandeur ni celle du défendeur',
        ),
      ),
    ),
    
    'information' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the information that you would like the Agency to consider in respect of the dispute proceeding.'),
      '@L[fr]#title' => "Indiquez clairement les renseignements que vous souhaitez que l'Office prenne en considération à l'égard de l'instance.",
      '#required' => TRUE,
    ),
    
    'day_aware' => array(
      '#type' => 'textarea',
      '#title' => t('Indicate the day on which you became aware of the application.'),
      '@L[fr]#title' => 'Indiquez la date à laquelle vous avez pris connaissance de la demande.',
      '#required' => TRUE,
    ),
        
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your intervention, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre intervention, vous devez les déposer le même jour que votre intervention.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

