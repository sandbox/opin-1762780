<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>An interested person.</p>
<h2>Purpose</h2>
<p>To provide comments on a dispute proceeding before the Agency.</p>
<h2>When should you file this form?</h2>
<p>Before the close of pleadings. Prior approval from the Agency is not required to file a position statement.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-6" target="_blank">section 23</a> of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>The position statement will be placed on the public record unless a request for confidentiality is made in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a> and accepted by the Agency. The Agency will consider your position statement during its decision-making process.</p>
<p>A person who files a position statement is not a party to the dispute proceeding. You will have no participation rights in the proceeding and you are not entitled to be copied on any further information or submissions filed in the proceeding. &nbsp;</p>
<p>You may, however, be required to respond to questions or information requests from the Agency or, upon request and with prior approval from the Agency, from a party to the dispute proceeding.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Toute personne intéressée.</p>
<h2>But</h2>
<p>Fournir des commentaires à l’égard d’une instance de règlement d’un différend devant l’Office.&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Avant la clôture des actes de procédure. Il n’est pas nécessaire d’obtenir l’approbation de l’Office pour déposer un énoncé de position.</p>
<p>Veuillez consulter l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-6\" target=\"_blank\">article 23</a> des Règles pour le règlement des différends pour de plus amples renseignements.&nbsp;<strong></strong></p>
<h2>Quelle est la prochaine étape?</h2>
<p>L’énoncé de position sera versé aux archives publiques à moins qu’une requête de confidentialité soit présentée en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a> et acceptée par l’Office. L’Office&nbsp; prendra en considération votre énoncé de position au cours du processus décisionnel. <strong></strong></p>
<p>Une personne qui dépose un énoncé de position n’est pas une partie à l’instance. Vous n’aurez aucun droit de participation, ni le droit de recevoir tout autre renseignement ou document présenté dans le cadre de l’instance. &nbsp;</p>
<p>Vous pourriez toutefois devoir répondre à des questions ou à des demandes de renseignements de l’Office ou, sur demande et suivant l’approbation de l’Office, d’une partie à l'instance de règlement du différend.</p>";


  $person_filing_form_options = array(
    '1' => t("I'm filing on behalf of myself"),
    '2' => t("I'm filing on behalf myself and other individuals"),
    '3' => t("I'm filing on behalf one or more organizations"),
    '4' => t("I'm a lawyer"),
    '5' => t("I'm a family member, friend, etc., who is representing the person filing a position statement.")
  );
  
  $person_filing_form_options_fr = array(
    '1' => "Je dépose le formulaire en mon nom.",
    '2' => "Je dépose le formulaire en mon nom et au nom d'autres personnes.",
    '3' => "Je dépose le formulaire au nom d'un ou de plusieurs organismes.",
    '4' => "Je suis un avocat.",
    '5' => "Je suis un membre de la famille, un ami, etc., qui représente la personne qui dépose un énoncé de position.",
  );
  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Contact Information'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Coordonnées',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    // The fieldset for the first radio button select box
    'fieldset_person_filing_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Who is filing this form?'),
      // Translates the title attribute when in french
      '@L[fr]#title' => 'Qui dépose ce formulaire?',
      
      
      // The first radio button select box
      'person_filing_form' => array(
        //'#title' => '',
        //'@L[fr]#title' => '',
        '#type' => 'radios',
        '#options' => $person_filing_form_options,
        '@L[fr]#options' => $person_filing_form_options_fr,
        '#required' => TRUE,
        // Send the result of this field via ajax
        '@ajax_send' => TRUE,
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'build_action' => 'dynamic_forms_variable_set',
          'build_action_args' => array(
            array(
              'variable' => 'applicant_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'representative_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'organization_contact_copies',
              'value' => 1,
            ),
          ),
        ),
      ),
     ),
    
    
    
    'how_to_fill_multi' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Person filing a position statement" section below.'
                     .'<br>Add the contact information for the other people by clicking the "Add another person" button.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Personne qui dépose un énoncé de position » ci-dessous.  Ajoutez les coordonnées de toute autre personne en cliquant sur le bouton « Ajoutez une autre personne ».",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '2',
      ),
    ),
    
    'how_to_fill_org' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'If you are filing this statement on behalf of a company, an association or other organization, identify yourself and provide the contact information for the organization.',
        '@L[fr]#markup' => "Si vous déposez cet énoncé au nom d'une entreprise, d'une association ou d'un autre organisme, identifiez-vous et fournissez les coordonnées de l'organisme.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '3',
      ),
    ),
    
    'how_to_fill_fam' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Representative" section.'
                    .'<br>Add the contact information for whoever you\'re representing in the "Person filing a position statement" section below.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Représentant ».  Ajoutez les coordonnées de toute personne dont vous êtes le représentant dans la section « Personne qui dépose un énoncé de position » ci-dessous.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '5',
      ),
    ),
    
    'representatives' => array(
      '@extends' => 'representative_contact_group',
    ),
    
    'organizations' => array(
      '@extends' => 'organization_contact_group',
    ),
    
    'lawyer' => array(
      '@extends' => 'lawyer_contact_group',
    ),
    
    'applicants' => array(
      'contact_info' => array(
        '@extends' => 'contact_info_required',
        '#type' => 'fieldset',
        '@#title' => array(
          'condition' => array(
            'value1' => '<B:applicant_contact_copies>',
            'operator' => '==',
            'value2' => 1,
            'eval_time' => 'B',
          ),
          'source_true' => t('Contact information: Person filing a position statement'),
          'source_false' => t('Contact information: Person filing a position statement') . '[B: applicant_contact_counter]',
        ),
        '@L[fr]@#title' => array(
          'condition' => array(
            'value1' => '<B:applicant_contact_copies>',
            'operator' => '==',
            'value2' => 1,
            'eval_time' => 'B',
          ),
          'source_true' => 'Coordonnées : personne qui dépose un énoncé de position',
          'source_false' => 'Coordonnées : personne qui dépose un énoncé de position' . '[B: applicant_contact_counter]',
        ),
        
        'consent' => array(
          '@extends' => 'description_text',
          '#markup' => "<p>We need the person's consent for you to act on their behalf. We will send you a pre-filled consent form automatically once you submit this form. The Agency will not be able to process the application until it receives the signed consent form.</p>",
          '@L[fr]#markup' => "<p>La déclaration de la personne qui vous autorise à agir en son nom doit nous être fournie. Nous vous ferons automatiquement parvenir un formulaire de consentement prérempli, une fois que vous aurez soumis ce formulaire. L'Office ne sera pas en mesure de traiter la demande avant d'avoir reçu le formulaire de consentement signé.</p>",
          '@dependencies' => array(
            'value1' => '{person_filing_form}',
            'operator' => '==',
            'value2' => '5',
          ),
          '#weight' => -10,
        ),
        
        'same_as' => array(
          '#title' => t('Same address as representative'),
          '@L[fr]#title' => 'Même adresse que celle du représentant',
          '#type' => 'checkbox',
          '@ajax_send' => TRUE,
          '#ajax' => array(
            'render_action' => 'dynamic_forms_same_as',
            'render_action_args' => array(
              'applicant_street_address#' => 'representative_street_address',
              'applicant_city#' => 'representative_city',
              'applicant_province#' => 'representative_province',
              'applicant_postal_code#' => 'representative_postal_code',
              'applicant_country#' => 'representative_country',
            ),
          ),
        ),
      ),
      '@extends' => 'applicant_contact_group',
        
      'remove_button' => array(
        '#value' => t('-Remove person'),
        '@L[fr]#value' => 'Supprimer une personne',
      ),
      'add_button' => array(
        '#value' => t('-Add another person'),
        '@L[fr]#value' => 'Ajouter une autre personne',
      ),
    ),
    
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'position statement',
      '<form_short_name_fr>' => 'énoncé de position',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '#markup' => t('Part 2 of 2: Details of the Application'),
      '@L[fr]#markup' => 'Partie 2 de 2 : Détails relatifs à la demande',
    ),
    
    'fieldset_support_position' => array(
      '#type' => 'fieldset',
      '#title' => t('I support the position of:'),
      '@L[fr]#title' => 'J\'appuie la position :',
      
      'support_position' => array(
        '#title' => '',
        '@L[fr]#title' => '',
        '#required' => TRUE,
        '#type' => 'radios',
        //'@required_label' => '',
        '#options' => array(
          'appl' => t('The applicant'),
          'resp' => t('The respondent'),
          'neit' => t('Neither the applicant nor the respondent'),
        ),
        '@L[fr]#options' => array(
          'appl' => 'Du demandeur',
          'resp' => 'Du défendeur',
          'neit' => 'Je n\'appuie ni la position du demandeur ni celle du défendeur',
        ),
      ),
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the arguments in support of your response.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre réponse.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your position statement, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre énoncé de position, vous devez les déposer le même jour que votre énoncé de position.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
    'personal_info' => array(
      '@extends' => 'personal_info',
    )
  );
    
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    )
  );
  
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;

  return $formArray;
}

/**
 * Check requirements
 */
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    $field_value = get_value('{person_filing_form}');

    if ($field_value == 4 OR $field_value == 5) {
        
      /*
      $contact_elements = array(
        'applicant_first_name',
        'applicant_last_name',
        'applicant_street_address',
        'applicant_city',
        'applicant_province',
        'applicant_postal_code',
        'applicant_country',
        'applicant_email',
        'applicant_phone_number',
      );

      foreach ($contact_elements as $contact) {
        if (strpos($field, $contact) !== FALSE) {
          return TRUE;
        }
      }
       */
      if (strpos($field, 'applicant_email') !== FALSE) {
        return FALSE;
      }
    }
  }
}


function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    // Get value of person filling form if available
    $field_value = get_value('{person_filing_form}');
      
    // Set flag to create auth pdf if necessary
    if ($field_value == 5) {
      set_system_variable('create_auth_pdf', TRUE);
    }
    else {
      set_system_variable('create_auth_pdf', FALSE);
    }
    
    if ($field == 'applicants') {
      // Evaluate result
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value != 3);
      }
    }   
    elseif ($field == 'organizations') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 3);
      }
    }
    elseif ($field == 'lawyer') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 4);
      }
    }
    elseif ($field == 'representatives') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 5);
      }
    }
    elseif ($field == 'respondents') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }    
    elseif ($field == 'applicant_add_button') {
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2);
      }
      else {
        return FALSE;
      }
    }
    elseif ($field == 'applicant_remove_button') {
      if (isset($result) AND $result === FALSE) {
        return FALSE;
      }
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2);
      }
      else {
        return FALSE;
      }
    }
    elseif (strpos($field, 'applicant_organization') !== FALSE) {
      if ($field_value == 4 OR $field_value == 5) {
        return TRUE;
      }
    }
    elseif ($field == 'applicant_same_as1') {
      if (isset($field_value)) {
        return ($field_value == 5);
      }
    }
  }
}

