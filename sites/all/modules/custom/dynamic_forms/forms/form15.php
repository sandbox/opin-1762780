<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person or party who filed a request.</p>
<table class="table-simplify" id="table-1002">
<thead>
<tr><th id="table-1002-r1-c1" scope="col" style="width: 199px;">Type of request to which you are replying</th><th id="table-1002-r1-c2" scope="col" width="127">Reference</th><th id="table-1002-r1-c3" scope="col" width="144">Who can file a reply</th><th id="table-1002-r1-c4" scope="col" width="166">Time limit for filing reply</th></tr>
</thead>
<tbody>
<tr>
<td headers="table-1002-r1-c1" width="199">
<p>General request</p>
</td>
<td headers="table-1002-r1-c2" width="127">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-1" target="_blank">subsection 27(3)</a></p>
</td>
<td headers="table-1002-r1-c3" width="144">
<p>Any person or party</p>
</td>
<td headers="table-1002-r1-c4" width="166">
<p>Within 2 business days after the day on which you receive a copy of the response.</p>
</td>
</tr>
<tr>
<td headers="table-1002-r1-c1" width="199">
<p>Request for expedited process</p>
</td>
<td headers="table-1002-r1-c2" width="127">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-2" target="_blank">subsection 28(5)</a></p>
</td>
<td headers="table-1002-r1-c3" width="144">
<p>A party</p>
</td>
<td headers="table-1002-r1-c4" rowspan="4" style="width: 166px;">
<p>Within 1 business day after the day on which you receive a copy of the response.</p>
</td>
</tr>
<tr>
<td headers="table-1002-r1-c1" width="199">
<p>Request to extend or shorten time limit</p>
</td>
<td headers="table-1002-r1-c2" width="127">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-4" target="_blank">subsection 30(3)</a></p>
</td>
<td headers="table-1002-r1-c3" width="144">
<p>Any person or party</p>
</td>
</tr>
<tr>
<td headers="table-1002-r1-c1" width="199">
<p>Request to amend document</p>
</td>
<td headers="table-1002-r1-c2" width="127">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-7" target="_blank">subsection 33(3)</a></p>
</td>
<td headers="table-1002-r1-c3" width="144">
<p>Any person or party</p>
</td>
</tr>
<tr>
<td headers="table-1002-r1-c1" width="199">
<p>Request to file document whose filing is not otherwise provided for in Rules</p>
</td>
<td headers="table-1002-r1-c2" width="127">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-8" target="_blank">subsection 34(3)</a></p>
</td>
<td headers="table-1002-r1-c3" width="144">
<p>Any person or party</p>
</td>
</tr>
</tbody>
</table>
<h2>Purpose</h2>
<p>To reply to a response from a party about a request that you filed. The reply must not raise issues or arguments that are not addressed in the response or introduce new evidence.</p>
<p>Refer to subsections 27(3), 28(5), 30(3), 33(3) and 34(3) of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>The Agency will consider all the submissions and make a determination as to whether the request will be granted.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  
  
  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne ou une partie qui a déposé une requête.&nbsp;</p>
<table class=\"table-simplify\" id=\"table-1002\">
<thead>
<tr><th id=\"table-1002-r1-c1\" scope=\"col\" style=\"width: 199px;\">Type de requête à laquelle vous répliquez&nbsp;</th><th id=\"table-1002-r1-c2\" scope=\"col\" width=\"127\">Référence</th><th id=\"table-1002-r1-c3\" scope=\"col\" width=\"144\">Qui peut déposer une réplique</th><th id=\"table-1002-r1-c4\" scope=\"col\" width=\"166\">Délais prévus pour déposer une réplique</th></tr>
</thead>
<tbody>
<tr>
<td headers=\"table-1002-r1-c1\" width=\"199\">
<p>Requête générale</p>
</td>
<td headers=\"table-1002-r1-c2\" width=\"127\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-1\" target=\"_blank\"><span style=\"text-decoration: underline;\">paragraphe 27(3)</span></a></p>
</td>
<td headers=\"table-1002-r1-c3\" width=\"144\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1002-r1-c4\" width=\"166\">
<p>Dans les deux jours ouvrables suivant la date de réception de la copie de la copie de la réponse</p>
</td>
</tr>
<tr>
<td headers=\"table-1002-r1-c1\" width=\"199\">
<p>Requête de procédure accélérée</p>
</td>
<td headers=\"table-1002-r1-c2\" width=\"127\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-2\" target=\"_blank\">paragraphe 28(5)</a></p>
</td>
<td headers=\"table-1002-r1-c3\" width=\"144\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1002-r1-c4\" rowspan=\"4\" style=\"width: 166px;\">
<p>Au plus tard un jour ouvrable après la date de réception de la copie de la réponse<strong></strong></p>
</td>
</tr>
<tr>
<td headers=\"table-1002-r1-c1\" width=\"199\">
<p>Requête de prolongation ou d’abrégement de délais&nbsp;</p>
</td>
<td headers=\"table-1002-r1-c2\" width=\"127\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-4\" target=\"_blank\"><span>paragraphe</span>&nbsp;30(3)</a></p>
</td>
<td headers=\"table-1002-r1-c3\" width=\"144\">
<p>Toute personne ou partie</p>
</td>
</tr>
<tr>
<td headers=\"table-1002-r1-c1\" width=\"199\">
<p>Requête de modification de document&nbsp;</p>
</td>
<td headers=\"table-1002-r1-c2\" width=\"127\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-7\" target=\"_blank\"><span>paragraphe</span>&nbsp;33(3)</a></p>
</td>
<td headers=\"table-1002-r1-c3\" width=\"144\">
<p>Toute personne ou partie</p>
</td>
</tr>
<tr>
<td headers=\"table-1002-r1-c1\" width=\"199\">
<p>Requête de dépôt de document dont le dépôt n'est pas prévu par les règles&nbsp;</p>
</td>
<td headers=\"table-1002-r1-c2\" width=\"127\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-8\" target=\"_blank\"><span>paragraphe</span>34(3)</a></p>
</td>
<td headers=\"table-1002-r1-c3\" width=\"144\">
<p>Toute personne ou partie</p>
</td>
</tr>
</tbody>
</table>
<h2>But</h2>
<p>Répliquer à une réponse d’une partie au sujet d’une requête que vous avez déposée. La réplique ne doit pas soulever des questions ou des arguments qui ne sont pas abordés dans la réponse ni introduire une nouvelle preuve. <strong></strong></p>
<p>Veuillez consulter les paragraphes 27(3), 28(5), 30(3), 33(3) et 34(3) des Règles pour le règlement des différends pour de plus amples renseignements</p>
<h2>Quelle est la prochaine étape?</h2>
<p>L’Office examinera toutes les présentations et déterminera si la requête est accordée ou non.&nbsp;</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'reply to response to request',
      '<form_short_name_fr>' => 'réplique à la réponse à une requête',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs à la <form_short_name_fr>',
    ),
    
    'note' => array(
        '@extends' => 'attention_text2',
      'heading' => array(
        '#markup' => 'Note:',
        '@L[fr]#markup' => "Veuillez noter :",
      ),
      'body' => array(
        '#markup' => 'You must not raise any new issues, arguments or introduce new evidence that were not discussed in the response.',
        '@L[fr]#markup' => "Vous ne devez pas soulever des questions ou des arguments qui ne sont pas mentionnés dans la réponse ni introduire une nouvelle preuve dont il n'est pas fait mention dans la réponse.",
      ),
    ),
    
    'specify_request' => array(
      '#type' => 'textarea',
      '#title' => t('Specify which response you are replying to, including the name of the person who filed the response.'),
      '@L[fr]#title' => 'Précisez à quelle réponse vous répliquez, y compris le nom de la personne qui a déposé la réponse.',
      '#required' => TRUE,
    ),
    
    
    'fieldset_responding' => array(
      '#type' => 'fieldset',
      '#title' => t('Do you agree with the response?'),
      '@L[fr]#title' => "Êtes-vous d'accord avec la réponse?",
      
      'responding' => array(
        '#required' => TRUE,
        '#type' => 'radios',
        '@ajax_send' => TRUE,
        //'@required_label' => '',
        '#options' => array(
          'Y' => t('Yes'),
          'N' => t('No'),
        ),
        '@L[fr]#options' => array(
          'Y' => 'Oui',
          'N' => 'Not',
        ),
      ),
    ),
    
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Set out the arguments in support of your position.'),
      '@L[fr]#title' => "Énoncez les arguments à l'appui de votre position.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your reply, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réplique, vous devez les déposer le même jour que votre réplique.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

