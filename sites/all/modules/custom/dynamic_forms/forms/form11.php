<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>Any party that needs more information to make submissions in the dispute proceeding.</p>
<h2>Purpose</h2>
<p>To direct written questions or to ask for documents to be produced by any other party that is adverse in interest.</p>
<h2>When should you file this form?</h2>
<p>For written questions, any time before the close of pleadings.</p>
<p>For document production, within 5 business days after the day on which you become aware of the document or before the close of pleadings, whichever is earlier.</p>
<h2>What happens next?</h2>
<p>The party that receives a notice asking them to respond to written questions or produce documents will have 5 business days after the day on which they receive a copy of the notice to respond. The party must either provide a complete response to each question or a copy of any document requested, or object to the request or part of the request.</p>
<p>If you are not satisfied with the response provided by the party or if you oppose the objection made by the party, you may file a request for the Agency to require the party to provide a complete response in accordance with <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-6" target="_blank">section 32</a>.</p>
<p>Refer to <a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-7" target="_blank">sections 24</a>&nbsp;and&nbsp;<a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-6">32</a>&nbsp;of the Dispute Adjudication Rules for more information.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Toute partie qui a besoin de plus de renseignements afin de faire une présentation dans le cadre de l’instance de règlement d'un différend. &nbsp;</p>
<h2>But</h2>
<p>Demander à toute autre partie qui a des intérêts opposés aux siens de répondre à des questions écrites ou de produire des documents.&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Pour les questions écrites, avant la clôture des actes de procédure. <strong></strong></p>
<p>Pour la production de documents, dans les cinq jours ouvrables suivant la date à laquelle vous êtes informé de son existence ou avant la clôture des actes de procédure, selon la première éventualité.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>La partie à qui est envoyé l'avis de questions écrites ou de production de document doit, dans les cinq jours ouvrables suivant la date de réception de la copie de l’avis, déposer une réponse complète à chacune des questions ou produire les documents demandés, selon le cas, ou s’opposer à la demande, en tout ou en partie. <strong></strong></p>
<p>Si vous n'êtes pas satisfait de la réponse fournie par la partie ou si vous contestez son opposition, vous pouvez déposer une requête auprès de l’Office visant à obliger une partie à fournir une réponse complète à l'avis, en vertu de l’<a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-6\" target=\"_blank\">article 32</a>.</p>
<p>Veuillez consulter les <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-7\" target=\"_blank\">articles 24</a> et <a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-6\" target=\"_blank\">32</a> des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
    
    'up_to_date_contact_info' => array(
      '@extends' => 'up_to_date_contact_info',
    ),
    
    
  );




  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'written questions or request for documents',
      '<form_short_name_fr>' => 'questions écrites ou demande de documents',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs aux <form_short_name_fr>',
    ),
    
    'fieldset_requesting' => array(
      '#type' => 'fieldset',
      '#title' => t('What are you requesting?'),
      '@L[fr]#title' => "Quel est l'objet de votre requête?",
      
      'requesting' => array(
        '#required' => TRUE,
        '#type' => 'radios',
        '@ajax_send' => TRUE,
        //'@required_label' => '',
        '#options' => array(
          'questions' => t('I want to direct written questions to a party'),
          'documents' => t('I want to request documents'),
        ),
        '@L[fr]#options' => array(
          'questions' => 'Je souhaite adresser des questions écrites à une partie.',
          'documents' => 'Je souhaite demander des documents.',
        ),
      ),
    ),
    
    
    'questions' => array(
      '#type' => 'fieldset',
      '#title' => 'Questions',
      '@L[fr]#title' => 'Questions',
      
      '@dependencies' => array(
        'value1' => '{requesting}',
        'operator' => '==',
        'value2' => 'questions',
      ),
      
      'ident_questions' => array(
        '#type' => 'textarea',
        '#title' => t('Identify the party to whom the written questions are directed'),
        '@L[fr]#title' => "Dressez une liste des questions écrites.",
        '#required' => TRUE,
      ),
      'list_questions' => array(
        '#type' => 'textarea',
        '#title' => t('List the written questions'),
        '@L[fr]#title' => 'Indiquez les éléments dans la réponse avec lesquels vous êtes en désaccord.',
        '#required' => TRUE,
      ),
      'explain_questions' => array(
        '#type' => 'textarea',
        '#title' => t('Explain the relevance of the questions to the dispute proceeding'),
        '@L[fr]#title' => "Expliquez la pertinence des questions à l'instance de règlement des différends.",
        '#required' => TRUE,
      ),
    ),
    
    'documents' => array(
      '#type' => 'fieldset',
      '#title' => 'Request for documents',
      '@L[fr]#title' => 'Demande de documents',
      
      '@dependencies' => array(
        'value1' => '{requesting}',
        'operator' => '==',
        'value2' => 'documents',
      ),
      
      'ident_documents' => array(
        '#type' => 'textarea',
        '#title' => t('Identify the party to whom the document request is directed'),
        '@L[fr]#title' => "Indiquez le nom de la partie à qui s'adresse la demande de documents.",
        '#required' => TRUE,
      ),
      'list_documents' => array(
        '#type' => 'textarea',
        '#title' => t('List the documents requested'),
        '@L[fr]#title' => 'Dressez une liste des documents demandés.',
        '#required' => TRUE,
      ),
      'explain_documents' => array(
        '#type' => 'textarea',
        '#title' => t('Explain the relevance of the documents to the dispute proceeding'),
        '@L[fr]#title' => "Expliquez la pertinence des documents à l'instance.",
        '#required' => TRUE,
      ),
    ),
    
    
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your request, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre demande, vous devez les déposer le même jour que votre demande.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

