<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person or party who wishes to make a general or specific request to the Agency.</p>
<table class="table-simplify" id="table-1000">
<thead>
<tr><th id="table-1000-r1-c1" scope="col" width="151">Type of request</th><th id="table-1000-r1-c2" scope="col" width="102">Reference</th><th id="table-1000-r1-c3" scope="col" width="150">Who can make the request</th><th id="table-1000-r1-c4" scope="col" width="324">Time limit for filing request</th></tr>
</thead>
<tbody>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>General request</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-1" target="_blank">subsection 27(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>Any person or party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>As soon as feasible, and, in any event, before the close of pleadings</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request for expedited process</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-2" target="_blank">subsection 28(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>A party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>If the request is to have the expedited process apply to an answer and a reply,</p>
<p>(i) in the case of an applicant, at the time that the application is filed, or</p>
<p>(ii) in the case of a respondent, within one business day after the date of the notice indicating that the application has been accepted.</p>
<p>If the request is to have the expedited process apply to another request,</p>
<p>(i) in the case of a person filing the other request, at the time that that request is filed, or</p>
<p>(ii) in the case of a person responding to the other request, within one business day after the day on which they receive a copy of that request.</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to extend or shorten time limit</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-4" target="_blank">subsection 30(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>Any person or party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>As soon as you are aware of the need to extend or shorten time limits</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to require party to provide complete response</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-6" target="_blank">subsection 32(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>A party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>Within two business days after the day on which you receive a copy of the response</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to amend document</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-7" target="_blank">subsection 33(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>Any person or party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>Any time before the close of pleadings</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to file document whose filing is not otherwise provided for in Rules</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-8" target="_blank">subsection 34(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>Any person or party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>As soon as you are aware of the need to file documents outside the established pleadings process</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to withdraw document</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-9" target="_blank">subsection 35(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>Any person or party</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>Before the close of pleadings</p>
</td>
</tr>
<tr>
<td headers="table-1000-r1-c1" width="151">
<p>Request to withdraw application</p>
</td>
<td headers="table-1000-r1-c2" width="102">
<p><a href="http://otc-cta.gc.ca/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-10" target="_blank">subsection 36(1)</a></p>
</td>
<td headers="table-1000-r1-c3" width="150">
<p>An applicant</p>
</td>
<td headers="table-1000-r1-c4" width="324">
<p>Before a final decision is made by the Agency in respect of the application</p>
</td>
</tr>
</tbody>
</table>
<h2>Purpose</h2>
<p>To submit a general request to the Agency to decide on procedural or other issues within a dispute proceeding or to submit one of the specific requests set out above.</p>
<p>Refer to subsections 27(1), 28(1), 30(1), 32(1), 33(1), 34(1), 35(1) and 36(1) of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>An opportunity may be given to a person or party to respond and for you to file a reply, depending on the section under which the request is made.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://otc-cta.gc.ca/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  
  
  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne ou une partie qui souhaite déposer une requête générale ou spécifique auprès de l’Office.&nbsp;</p>
<table class=\"table-simplify\" id=\"table-1000\">
<thead>
<tr><th id=\"table-1000-r1-c1\" scope=\"col\" width=\"151\">Type de requête</th><th id=\"table-1000-r1-c2\" scope=\"col\" width=\"102\">Référence</th><th id=\"table-1000-r1-c3\" scope=\"col\" width=\"150\">Qui peut déposer une requête</th><th id=\"table-1000-r1-c4\" scope=\"col\" width=\"324\">Délais prévus pour le dépôt d’une requête</th></tr>
</thead>
<tbody>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête générale</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-1\" target=\"_blank\">paragraphe 27(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Le plus tôt possible et, dans tous les cas, avant la clôture des actes de procédure&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de procédure accélérée</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-2\" target=\"_blank\">paragraphe 28(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Si la requête de procédure accélérée vise la réponse et la réplique&nbsp;:</p>
<p>(i) en ce qui concerne le demandeur, au moment du dépôt de la demande;</p>
<p>(ii) en ce qui concerne le défendeur, au plus tard un jour ouvrable suivant la date de l’avis d’acceptation de la demande.</p>
<p>Si la requête de procédure accélérée vise une autre requête&nbsp;:</p>
<p>(i) en ce qui concerne la personne qui dépose cette autre requête, au moment du dépôt de celle-ci;</p>
<p>(ii) en ce qui concerne la personne qui répond à cette autre requête, au plus tard un jour ouvrable après la date de réception de la copie de celle-ci.</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de prolongation ou d’abrégement de délais&nbsp;</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-4\" target=\"_blank\">paragraphe 30(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Aussitôt que vous êtes conscient du besoin de prolonger ou d’abréger les délais&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête visant à obliger une partie à fournir une réponse complète à l'avis</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-6\" target=\"_blank\">paragraphe 32(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Dans les deux jours ouvrables suivant la date de réception de la copie de la réponse</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de modification de document&nbsp;</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-7\" target=\"_blank\"><span>paragraphe</span>&nbsp;33(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>À tout moment avant la clôture des actes de procédure</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de dépôt de document dont le dépôt n'est pas prévu par les règles&nbsp;</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-8\" target=\"_blank\"><span>paragraphe</span>&nbsp;34(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Aussitôt que vous êtes conscient du besoin de déposer des documents en dehors du processus établi des actes de procédure&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de retrait d’un document</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-9\" target=\"_blank\"><span>paragraphe</span>&nbsp;35(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Toute personne ou partie</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Avant la clôture des actes de procédure&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1000-r1-c1\" width=\"151\">
<p>Requête de retrait d’une demande</p>
</td>
<td headers=\"table-1000-r1-c2\" width=\"102\">
<p><a href=\"http://otc-cta.gc.ca/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-10\" target=\"_blank\"><span>paragraphe</span>&nbsp;36(1)</a></p>
</td>
<td headers=\"table-1000-r1-c3\" width=\"150\">
<p>Un demandeur</p>
</td>
<td headers=\"table-1000-r1-c4\" width=\"324\">
<p>Avant que l'Office ne rende une décision définitive relative à la demande&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<h2>But</h2>
<p>Présenter une requête générale à l’Office pour une décision sur des questions de procédure ou autres dans le cadre d’une instance de règlement d’un différend <strong>ou </strong>présenter une des requêtes spécifiques énoncées ci-haut.</p>
<p>Veuillez consulter les paragraphes 27(1), 28(1), 30(1), 32(1), 33(1), 34(1), 35(1) et 36(1) des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>Une personne ou une partie pourrait avoir la possibilité de répondre et vous de déposer une réplique, selon l’article en vertu duquel la requête est présentée.&nbsp;</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://otc-cta.gc.ca/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
    
    'up_to_date_contact_info' => array(
      '@extends' => 'up_to_date_contact_info',
    ),
    
    
  );




  // Define the second page
  $page2 = array(
    
    '#type' => 'group',
    '@variables' => array(
      '<form_short_name>' => 'request',
      '<form_short_name_fr>' => 'requête',
    ),
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs aux <form_short_name_fr>',
    ),
    'describe_request' => array(
      '#type' => 'textarea',
      '#title' => t('Describe the details of your request, including any relief requested'),
      '@L[fr]#title' => 'Décrivez les détails de votre requête, y compris la réparation demandée.',
      '#required' => TRUE,
    ),
    'summary_facts' => array(
      '#type' => 'textarea',
      '#title' => t('Set out a summary of the facts '),
      '@L[fr]#title' => 'Donnez un résumé des faits.',
      '#required' => TRUE,
    ),
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Set out the arguments in support of your request'),
      '@L[fr]#title' => "Fournissez les arguments à l'appui de votre requête.",
      '#required' => TRUE,
    ),
    'request_expedited' => array(
      '@extends' => 'info_text2',
      'heading' => array(
        '#markup' => 'Are you making a request for an expedited process under subsection 28(1)?',
        '@L[fr]#markup' => "Déposez-vous une requête en vue de demander l'application de la procédure accélérée en vertu du paragraphe 28(1)? ",
      ),
      'body' => array(
        '#markup' => 'Make sure that your arguments demonstrate that adherence to the ordinary time limits would cause financial or other prejudice.',
        '@L[fr]#markup' => "Assurez-vous que vos arguments démontrent le préjudice financier ou autre qui vous serait causé si les délais établis étaient appliqués.",
      ),
    ),
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your request, you must file them on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre requête, vous devez les déposer le même jour que votre requête.",
      ),
      
      'request_amend_docs' => array(
        '@extends' => 'info_text2',
        'heading' => array(
          '#markup' => 'Are you requesting to amend documents under subsection 33(1) or submit documents whose filing is not provided for in the Rules under subsection 34(1)?',
          '@L[fr]#markup' => "Déposez-vous une requête de modification de documents en vertu du paragraphe 33(1) ou une requête de dépôt de documents non prévu par les Règles en vertu de l'alinéa 34(1)?",
        ),
        'body' => array(
          '#markup' => 'You must include a copy of the documents that you want to file.',
          '@L[fr]#markup' => "Vous devez inclure une copie des documents que vous souhaitez déposer.",
        ),
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

