<?php

require_once __DIR__.'/../forms_decl_class.php';

DF\form_list::$forms_declaration['form5'] = array(
  'title' => 'Form 5: Application',   // Title of form
  'file' => 'form5.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission-510',
  'auth_template' => 'grp-auth-eng.html',
  
  'translations' => array(
    'fr' => 'formulaire5',
  ),
);

DF\form_list::$forms_declaration['formulaire5'] = array(
  'title' => 'Formulaire 5 : Demande',   // Title of form
  'file' => 'form5.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission-510',
  'auth_template' => 'grp-auth-fra.html',
  
  'translations' => array(
    'en' => 'form5',
  ),
);


DF\form_list::$forms_declaration['form6'] = array(
  'title' => 'Form 6: Answer to application',   // Title of form
  'file' => 'form6.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  'auth_template' => 'grp-auth-eng.html',
  
  'translations' => array(
    'fr' => 'formulaire6',
  ),
);

DF\form_list::$forms_declaration['formulaire6'] = array(
  'title' => 'Formulaire 6 : Réponse à une demande',   // Title of form
  'file' => 'form6.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  'auth_template' => 'grp-auth-fra.html',
  
  'translations' => array(
    'en' => 'form6',
  ),
);


DF\form_list::$forms_declaration['form7'] = array(
  'title' => 'Form 7: Reply to answer',   // Title of form
  'file' => 'form7.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire7',
  ),
);

DF\form_list::$forms_declaration['formulaire7'] = array(
  'title' => 'Formulaire 7 : Réplique à la réponse',   // Title of form
  'file' => 'form7.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form7',
  ),
);

DF\form_list::$forms_declaration['form8'] = array(
  'title' => 'Form 8: Intervention',   // Title of form
  'file' => 'form8.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire8',
  ),
);

DF\form_list::$forms_declaration['formulaire8'] = array(
  'title' => 'Formulaire 8 : Intervention',   // Title of form
  'file' => 'form8.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form8',
  ),
);

DF\form_list::$forms_declaration['form9'] = array(
  'title' => 'Form 9: Response to intervention',   // Title of form
  'file' => 'form9.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire9',
  ),
);

DF\form_list::$forms_declaration['formulaire9'] = array(
  'title' => 'Formulaire 9 : Réponse à l’intervention',   // Title of form
  'file' => 'form9.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form9',
  ),
);

DF\form_list::$forms_declaration['form10'] = array(
  'title' => 'Form 10: Position statement',   // Title of form
  'file' => 'form10.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission-510',
  'auth_template' => 'grp-auth-eng.html',
  
  'translations' => array(
    'fr' => 'formulaire10',
  ),
);

DF\form_list::$forms_declaration['formulaire10'] = array(
  'title' => 'Formulaire 10 : Énoncé de position',   // Title of form
  'file' => 'form10.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission-510',
  'auth_template' => 'grp-auth-fra.html',
  
  'translations' => array(
    'en' => 'form10',
  ),
);

DF\form_list::$forms_declaration['form11'] = array(
  'title' => 'Form 11: Written questions or document request',   // Title of form
  'file' => 'form11.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire11',
  ),
);

DF\form_list::$forms_declaration['formulaire11'] = array(
  'title' => 'Formulaire 11 : Questions écrites et demande de documents',   // Title of form
  'file' => 'form11.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form11',
  ),
);

DF\form_list::$forms_declaration['form12'] = array(
  'title' => 'Form 12: Response to written questions or document request',   // Title of form
  'file' => 'form12.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire12',
  ),
);

DF\form_list::$forms_declaration['formulaire12'] = array(
  'title' => 'Formulaire 12 : Réponse aux questions écrites ou demande de document',   // Title of form
  'file' => 'form12.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form12',
  ),
);

DF\form_list::$forms_declaration['form13'] = array(
  'title' => 'Form 13: Request',   // Title of form
  'file' => 'form13.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire13',
  ),
);

DF\form_list::$forms_declaration['formulaire13'] = array(
  'title' => 'Formulaire 13 : Requête',   // Title of form
  'file' => 'form13.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form13',
  ),
);

DF\form_list::$forms_declaration['form14'] = array(
  'title' => 'Form 14: Response to request',   // Title of form
  'file' => 'form14.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire14',
  ),
);

DF\form_list::$forms_declaration['formulaire14'] = array(
  'title' => 'Formulaire 14 : Réponse à une requête',   // Title of form
  'file' => 'form14.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form14',
  ),
);

DF\form_list::$forms_declaration['form15'] = array(
  'title' => 'Form 15: Reply to response to request',   // Title of form
  'file' => 'form15.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire15',
  ),
);

DF\form_list::$forms_declaration['formulaire15'] = array(
  'title' => 'Formulaire 15 : Réplique à la réponse à une requête',   // Title of form
  'file' => 'form15.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form15',
  ),
);

DF\form_list::$forms_declaration['form16'] = array(
  'title' => 'Form 16: Request to intervene',   // Title of form
  'file' => 'form16.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  'auth_template' => 'grp-auth-eng.html',
  
  'translations' => array(
    'fr' => 'formulaire16',
  ),
);

DF\form_list::$forms_declaration['formulaire16'] = array(
  'title' => 'Formulaire 16 : Requête pour intervention',   // Title of form
  'file' => 'form16.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  'auth_template' => 'grp-auth-fra.html',
  
  'translations' => array(
    'en' => 'form16',
  ),
);

DF\form_list::$forms_declaration['form17'] = array(
  'title' => 'Form 17: Request for confidentiality',   // Title of form
  'file' => 'form17.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire17',
  ),
);

DF\form_list::$forms_declaration['formulaire17'] = array(
  'title' => 'Formulaire 17 : Requête de confidentialité',   // Title of form
  'file' => 'form17.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form17',
  ),
);

DF\form_list::$forms_declaration['form18'] = array(
  'title' => 'Form 18: Request for disclosure',   // Title of form
  'file' => 'form18.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Required',  // changed the default marker label
  'lang' => 'en',
  'post_submit_redirect' => 'we-have-successfully-received-your-submission',
  
  'translations' => array(
    'fr' => 'formulaire18',
  ),
);

DF\form_list::$forms_declaration['formulaire18'] = array(
  'title' => 'Formulaire 18 : Requête de divulgatione',   // Title of form
  'file' => 'form18.php',              // Path/Filename of the form
  'function' => 'DF\get_form',        // Name of form function
  'default_required_label' => 'Obligatoire',  // changed the default marker label
  'lang' => 'fr',
  'post_submit_redirect' => 'nous-avons-bien-recu-votre-soumission',
  
  'translations' => array(
    'en' => 'form18',
  ),
);
