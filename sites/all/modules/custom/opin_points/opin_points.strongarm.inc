<?php
/**
 * @file
 * opin_points.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function opin_points_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'userpoints_category_default_vid';
  $strongarm->value = '2';
  $export['userpoints_category_default_vid'] = $strongarm;

  return $export;
}
