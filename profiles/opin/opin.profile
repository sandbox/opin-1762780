<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function opin_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks_alter().
 * This function alters the installation steps to push custom steps where user will see the module selection form and 
 * module configuration form if enabled
 * More information about this hook in this link https://api.drupal.org/api/function/hook_install_tasks_alter/7
 */
function opin_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_select_profile']['display'] = FALSE;
  // Set the installation theme.
  _opin_profile_set_theme('adminimal');
  
  /**
   * This section will push the module selection form to tasks list
   */
  $old_tasks = $tasks;
  $key_index = array_search('install_select_locale', array_keys($old_tasks));
  
  $new_tasks['opin_module_enable_form'] = array(
        'display_name' => st('Select modules'),
        'display' => TRUE,
        'type' => 'form',
        'run' => isset($install_state['parameters']['module_enable']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
      );
  
  $tasks = array_slice($old_tasks, 0, $key_index + 1) + $new_tasks + array_slice($old_tasks, $key_index);
  
  $old_tasks = $tasks;
  $key_index = array_search('opin_module_enable_form', array_keys($old_tasks));
  
  $new_tasks['opin_depend_list_form'] = array(
        'display_name' => st('Dependent module list'),
        'display' => TRUE,
        'type' => 'form',
        'run' => isset($install_state['parameters']['module_depend_list']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
      );
  
  $tasks = array_slice($old_tasks, 0, $key_index + 1) + $new_tasks + array_slice($old_tasks, $key_index);
}

/**
 * Force-set a theme at any point during the execution of the request.
 *
 * Drupal doesn't give us the option to set the theme during the installation
 * process and forces enable the maintenance theme too early in the request
 * for us to modify it in a clean way.
 */
function _opin_profile_set_theme($target_theme) {
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);

    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }
}

/**
 * This function creates module selection form and shows module list by reading available modules from directory 'profiles/opin/modules'
 * @param type $form
 * @param type $form_state
 * @param type $install_state
 * @return type
 */
function opin_module_enable_form($form, &$form_state, &$install_state) {
  drupal_set_title(st('Select modules'));
  
  $module_custom = scandir('sites/all/modules/custom');
  $module_contrib = scandir('sites/all/modules/contrib');
  $module_core = scandir('modules');
  $module_list = array();
  $avail_modules = array();
  $avail_modules_core = array();
  $avail_modules_custom = array();
  $depend_modules_list = array();
  
  /**
   * This section is getting all available custom modules for dependency check later
   */
  foreach($module_custom as $module_custom_key => $module_custom_value) {
    if($module_custom_value != '.' && $module_custom_value != '..') {
      $file_dir_name_module_custom = file_scan_directory('sites/all/modules/custom/' . $module_custom_value, '/.*\.info$/');
      foreach($file_dir_name_module_custom as $file_path_module_custom => $file_info_module_custom) {
        array_push($avail_modules_custom, $file_info_module_custom->name);
      }
    }
  }
  
  /**
   * This section is getting all available core modules so that we can discard those for dependency check later
   */
  foreach($module_core as $module_core_key => $module_core_value) {
    if($module_core_value != '.' && $module_core_value != '..') {
      $file_dir_name_module_core = file_scan_directory('modules/' . $module_core_value, '/.*\.info$/');
      foreach($file_dir_name_module_core as $file_path_module_core => $file_info_module_core) {
        array_push($avail_modules_core, $file_info_module_core->name);
      }
    }
  }
  
  /**
   * This section is getting all available contrib modules for dependency check later
   */
  foreach($module_contrib as $module_avail_key => $module_avail_value) {
    if($module_avail_value != '.' && $module_avail_value != '..') {
      $file_dir_name_avail = file_scan_directory('sites/all/modules/contrib/' . $module_avail_value, '/.*\.info$/');
      foreach($file_dir_name_avail as $file_path_avail => $file_info_avail) {
        array_push($avail_modules, $file_info_avail->name);
      }
    }
  }
  
  
  foreach($module_custom as $key => $value) {
    if($value != '.' && $value != '..') {
      $file_dir_name = file_scan_directory('sites/all/modules/custom/' . $value, '/.*\.info$/');
      
      foreach($file_dir_name as $file_path => $file_info) {
        $flag_missing = 0;
        $file_name = glob($file_path);
        $info = drupal_parse_info_file($file_name[0]);
      
        if(empty($info['version'])) {
          $info['version'] = 'Version number missing';
        }
        $dependencies = '';
        if(isset($info['dependencies']) && count($info['dependencies']) > 0) {
          foreach($info['dependencies'] as $key => $value) {
            $value = explode(" ", $value);
            $value = $value[0];
            if(!in_array($value, $avail_modules_core)) {
              if(!in_array($value, $avail_modules) && !in_array($value, $avail_modules_custom)) {
                $flag_missing = 1;
                $info['dependencies'][$key] = t('@module (<span class="admin-missing">missing</span>)', array('@module' => $value));
              }
              array_push($depend_modules_list, $value);
            }
          }
          $dependencies = '<br><div class="module-depend"> [Requires: ' . implode(', ', $info['dependencies']) . ']</div>';
        }
        $module_list[trim(basename($file_name[0], ".info") . PHP_EOL)] = $info['name'] . ' (' . $info['version'] . ')' . $dependencies;
        $module_list_depend_check[trim(basename($file_name[0], ".info") . PHP_EOL)] = $flag_missing;
        $module_list_depend[trim(basename($file_name[0], ".info") . PHP_EOL)] = $depend_modules_list;
        unset($depend_modules_list);
        $depend_modules_list = array();
      }
    }
  }
  
  $form['module'] = array(
    '#type' => 'fieldset',
    '#title' => st('Modules'),
  );
  
  $form['module']['module_list'] = array(
    '#title' => t('Module List'),
    '#description' => t('Only selected modules will be installed and enabled'),
    '#options' => $module_list,
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#required' => FALSE,
  );
  
  $form['#custom_data'] = $module_list_depend_check;
  $form['#custom_data_depend'] = $module_list_depend;
  
  foreach($module_list as $module_list_key => $module_list_value) {
    if($form['#custom_data'][$module_list_key] == 1) {
      $form['module']['module_list'][$module_list_key]['#disabled'] = TRUE;
    }
  }
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#weight' => 15,
  );
  
  return $form;
}

/**
 * This function will validate dependencies missing and checking required modules.
 * @param type $form
 * @param type $form_state
 */
function opin_module_enable_form_validate($form, &$form_state) {
  foreach($form_state['values']['module_list'] as $key => $value) {
    if($value != '0') {
      if($form['#custom_data'][$key] == 1) {
        form_set_error('module_list', st('Module dependencies are missing for selected module(s)'));
      }
      
      /*foreach($form_state['values']['module_list'] as $depend_key => $depend_value) {
        if($depend_value == '0') {
          if(in_array($depend_key, $form['#custom_data_depend'][$key])) {
            form_set_error('module_list', st('Please enable the modules required by your selected modules'));
          }
        }
      }*/
    }
  }
  //form_set_error('module_list', st('Please enter a valid email address'));
}

/**
 * This is the submit function for module enable form. It will write the selected modules in .info file after submission
 * @global type $install_state
 * @param type $form
 * @param type $form_state
 */
function opin_module_enable_form_submit(&$form, &$form_state) {
  global $install_state;
  $token_profile_divider = '; Install Opin dependencies.';
  
  if( strpos(file_get_contents("profiles/opin/opin.info"), $token_profile_divider) === false) {
    $new_file_content = file_get_contents('profiles/opin/opin.info');
  }
  else {
    $file_content = file_get_contents('profiles/opin/opin.info');
    $new_file_content = strstr($file_content, $token_profile_divider, true);
  }
  file_put_contents('profiles/opin/opin.info', $new_file_content);
  file_put_contents('profiles/opin/opin.info', $token_profile_divider . "\n", FILE_APPEND);
  foreach($form_state['values']['module_list'] as $key => $value) {
    if(trim($value) != '0') {
      $info_file = 'dependencies[] = ' . trim($value) . "\n";
      if( strpos(file_get_contents("profiles/opin/opin.info"), $info_file) === false) {
        file_put_contents('profiles/opin/opin.info', $info_file, FILE_APPEND);
      }
    }
  }

  $install_state['parameters']['module_enable'] = 'done';
  //$install_state['parameters']['locale'] = 'en';
}

function opin_depend_list_form($form, &$form_state, &$install_state) {
  drupal_set_title(st('List of dependencies'));
  
  $file_info = drupal_parse_info_file('profiles/opin/opin.info');
  
  $installed_module = array();
  foreach($file_info['dependencies'] as $key => $value) {
    array_push($installed_module, $value);
  }
  
  $module_custom = scandir('sites/all/modules/custom');
  $module_contrib = scandir('sites/all/modules/contrib');
  $module_core = scandir('modules');
  
  $avail_modules = array();
  $avail_modules_contrib = array();
  $avail_modules_core = array();
  $module_check = array();
  
  foreach($module_core as $module_core_key => $module_core_value) {
    if($module_core_value != '.' && $module_core_value != '..') {
      $file_dir_name_module_core = file_scan_directory('modules/' . $module_core_value, '/.*\.info$/');
      foreach($file_dir_name_module_core as $file_path_module_core => $file_info_module_core) {
        array_push($avail_modules_core, $file_info_module_core->name);
      }
    }
  }
  
  foreach($module_contrib as $key_m_contrib => $value_m_contrib) {
    if($value_m_contrib != '.' && $value_m_contrib != '..') {
      $file_dir_name_contrib = file_scan_directory('sites/all/modules/contrib/' . $value_m_contrib, '/.*\.info$/');
      foreach($file_dir_name_contrib as $file_path_avail_mod_contrib => $file_info_avail_mod_contrib) {
        array_push($avail_modules_contrib, $file_info_avail_mod_contrib->name);
      }
    }
  }
  
  $module_available = array();
  foreach($module_custom as $key_m => $value_m) {
    if($value_m != '.' && $value_m != '..') {
      $file_dir_name = file_scan_directory('sites/all/modules/custom/' . $value_m, '/.*\.info$/');
      /*foreach($file_dir_name as $file_path_avail_mod => $file_info_avail_mod) {
        array_push($avail_modules, $file_info_avail_mod->name);
      }*/
      foreach($file_dir_name as $file_path_avail => $file_info_avail) {
        array_push($avail_modules, $file_info_avail->name);
        if(in_array($file_info_avail->name, $installed_module)) {
          $file_name = glob($file_path_avail);
          $module_file_info = drupal_parse_info_file($file_name[0]);
          if(isset($module_file_info['dependencies'])) {
            foreach($module_file_info['dependencies'] as $key_depend => $key_value) {
              $key_value = explode(" ", $key_value);
              $key_value = $key_value[0];
              if(!in_array($key_value, $module_check)) {
                array_push($module_available, $key_value);
                $deep_depend_list = recursive_depend_list($key_value, array(), $module_available, array());
                foreach($deep_depend_list as $deep_key => $deep_value) {
                  array_push($module_available, $deep_value);
                }
                array_push($module_check, $key_value);
              }
            }
          }
        }
      }
    }
  }
  $module_available = array_unique($module_available);
  $dependencies_list = '';
  $flag_miss = 0;
  $module_option = array();
  $key_index = array();
  foreach($module_available as $module_available_key => $module_available_value) {
    $module_available_value = explode(" ", $module_available_value);
    $module_available_value = $module_available_value[0];
    if(!in_array($module_available_value, $avail_modules_core)) {
      if(!in_array($module_available_value, $avail_modules) && !in_array($module_available_value, $avail_modules_contrib)) {
        $dependencies_list .= '<li>' . $module_available_value . ' (<span class="admin-missing">missing</span>)</li>';
        $flag_miss = 1;
        $module_option[$module_available_value] = $module_available_value . ' (<span class="admin-missing">missing</span>)';
      }
      else {
        $dependencies_list .= '<li>' . $module_available_value . '</li>';
        $module_option[$module_available_value] = $module_available_value;
      }
      array_push($key_index, $module_available_value);
    }
  }
  
  $form['#prefix'] = '<div><h2>The following opin features will be installed.</h2>'.implode(", ", array_diff($installed_module, $avail_modules_core)).'</div>';
  
  $form['depend_module_list'] = array(
    '#title'         => t('The following modules will be automatically enabled'),
    '#options' => $module_option,
    '#type'          => 'checkboxes',
    '#disabled' => TRUE,
    '#default_value' => $key_index,
  );
  
  $form['#missing_flag'] = $flag_miss;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
    '#weight' => 15,
  );
  
  /**
   * This section is used to add extra button. You'll have to clone this section of code in every module config form as hook_form_alter 
   * is not supported in drupal installation profile. It supports form specific form alter means hook_FORM_ID_alter.
   */
  $destination = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $destination = substr($destination, 0, strripos($destination, '&'));

  $form['actions']['back'] = array(
    '#markup' => l(t('Back'), $destination),
    '#weight' => 20,
  );
  
  return $form;
}

function recursive_depend_list($module_name, $module_available = array(), $avail_modules = array(), $module_check = array()) {
  $modules = array_merge(scandir('sites/all/modules/custom'), scandir('sites/all/modules/contrib'));
  
  foreach($modules as $key_m => $value_m) {
    if($value_m != '.' && $value_m != '..') {
      $file_dir_name = array_merge(file_scan_directory('sites/all/modules/custom/' . $value_m, '/.*\.info$/'), 
          file_scan_directory('sites/all/modules/contrib/' . $value_m, '/.*\.info$/'));
      foreach($file_dir_name as $file_path_avail => $file_info_avail) {
        if($file_info_avail->name == $module_name) {
          $file_name = glob($file_path_avail);
          $module_file_info = drupal_parse_info_file($file_name[0]);
          if(isset($module_file_info['dependencies'])) {
            foreach($module_file_info['dependencies'] as $key_depend => $key_value) {
              $key_value = explode(" ", $key_value);
              $key_value = $key_value[0];
              if(!in_array($key_value, $module_check)) {
                if(!in_array($key_value, $avail_modules)) {
                  array_push($module_available, $key_value);
                  array_push($module_check, $key_value);
                  $module_available = array_merge(recursive_depend_list($key_value, $module_available, $avail_modules, $module_check), $module_available);
                }
              }
            }
          }
        }
      }
      
      /*$file_dir_name_contrib = file_scan_directory('sites/all/modules/contrib/' . $value_m, '/.*\.info$/');
      foreach($file_dir_name_contrib as $file_path_avail_contrib => $file_info_avail_contrib) {
        if($file_info_avail_contrib->name == $module_name) {
          $file_name_contrib = glob($file_path_avail_contrib);
          $module_file_info_contrib = drupal_parse_info_file($file_name_contrib[0]);
          if(isset($module_file_info_contrib['dependencies'])) {
            foreach($module_file_info_contrib['dependencies'] as $key_depend_contrib => $key_value_contrib) {
              if(!in_array($key_value_contrib, $avail_modules)) {
                array_push($module_available, $key_value_contrib);
                $module_available = array_merge(recursive_depend_list($key_value_contrib, $module_available, $avail_modules), $module_available);
              }
            }
          }
        }
      }*/
    }
  }
  return array_unique($module_available);
}

function opin_depend_list_form_validate($form, &$form_state) {
  if($form['#missing_flag'] == '1') {
    form_set_error('depend_module_list', st('Dependencies are missing'));
  }
}

function opin_depend_list_form_submit(&$form, &$form_state) {
  global $install_state;
  
  foreach($form_state['values']['depend_module_list'] as $key => $value) {
    if(trim($value) != '0') {
      $info_file = 'dependencies[] = ' . trim($value) . "\n";
      if( strpos(file_get_contents("profiles/opin/opin.info"), $info_file) === false) {
        file_put_contents('profiles/opin/opin.info', $info_file, FILE_APPEND);
      }
    }
  }

  $install_state['parameters']['module_depend_list'] = 'done';
}

/**
 * Configuration form for google analytics where it will take web analytics ID.
 * It will render configuration form of google analytics by calling the function from google analytics module.
 * See line no. 11 in googleanalytics.admin.inc file
 * @param type $form
 * @param type $form_state
 * @param type $install_state
 * @return string
 */
function opin_google_analytics_form($form, &$form_state, &$install_state) {
  drupal_set_title('Configure google analytics');
  module_load_include('inc', 'googleanalytics', 'googleanalytics.admin');
  return googleanalytics_admin_settings_form($form, $form_state);
}

/**
 * This function is used to add extra submit function to set parameter and to add back button as link in the google analytics config form
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function opin_form_opin_google_analytics_form_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = '_google_analytics_set_install_parameter';
  
  /**
   * This section is used to add extra button. You'll have to clone this section of code in every module config form as hook_form_alter 
   * is not supported in drupal installation profile. It supports form specific form alter means hook_FORM_ID_alter.
   */
  $destination = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $destination = substr($destination, 0, strripos($destination, '&'));

  $form['actions']['back'] = array(
    '#markup' => l(t('Back'), $destination),
    '#weight' => 20,
  );
}

/**
 * This function is setting the install parameter which will be shown in url. This is specially needed for back and forth operation.
 * @global array $install_state
 * @param type $form
 * @param type $form_state
 */
function _google_analytics_set_install_parameter($form, &$form_state) {
  global $install_state;
  $install_state['parameters']['ga'] = 'ga';
}
/**
 * End of google analytics config form operation
 */

/**
 * Implements hook_install_tasks().
 */
function opin_install_tasks(&$install_state) {
  /**
   * This section is for showing configuration form for different custom modules
   */
  if( strpos(file_get_contents("profiles/opin/opin.info"), 'dependencies[] = googleanalytics' . "\n") !== false) {
    $tasks['opin_google_analytics_form'] = array(
        'display_name' => st('Setup google analytics'),
        'display' => TRUE,
        'type' => 'form',
        'run' => isset($install_state['parameters']['ga']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
      );
  }
  
  /**
   * This section is for showing theme selection and config form respectively
   */
  $tasks['opin_theme_enable_form'] = array(
      'display_name' => st('Select Theme'),
      'type' => 'form',
      'display' => TRUE,
      'run' => isset($install_state['parameters']['theme_enable']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
    );
  
  $tasks['opin_theme_setting_form'] = array(
      'display_name' => st('Theme configuration'),
      'type' => 'form',
      'display' => TRUE,
      'run' => isset($install_state['parameters']['theme_setting']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
    );
  
  return $tasks;
}

/**
 * This function creates theme selection form and shows theme list by reading available themes from directory 'profiles/opin/themes'
 * @param type $form
 * @param type $form_state
 * @param type $install_state
 */
function opin_theme_enable_form($form, &$form_state, &$install_state) {
  drupal_set_title(st('Select themes'));
  
  $theme = scandir('sites/all/themes');
  $theme_list = array();

  foreach($theme as $key => $value) {
    if($value != '.' && $value != '..') {
      $file_dir_name = file_scan_directory('sites/all/themes/' . $value, '/.*\.info$/');
      foreach($file_dir_name as $file_path => $file_info) {
        $file_name = glob($file_path);
        $info = drupal_parse_info_file($file_name[0]);
        
        if(isset($info['version'])) {
          $theme_list[trim(basename($file_name[0], ".info") . PHP_EOL)] = $info['name'] . ' (' . $info['version'] . ')';
        }
        else {
          $theme_list[trim(basename($file_name[0], ".info") . PHP_EOL)] = $info['name'] . ' (Version number missing)';
        }
      }
    }
  }
  
  $form['theme'] = array(
    '#type' => 'fieldset',
    '#title' => st('Themes'),
  );
  
  $form['theme']['theme_list'] = array(
    '#title' => t('Select default theme'),
    '#description' => t('Only selected theme will be set as default'),
    '#options' => $theme_list,
    '#type' => 'radios',
    '#required' => FALSE,
  );
  
  $form['theme']['theme_admin_list'] = array(
    '#title' => t('Select administration theme'),
    '#description' => t('Only selected theme will be set as default'),
    '#options' => $theme_list,
    '#type' => 'radios',
    '#required' => FALSE,
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#weight' => 15,
  );
  
  /**
   * As I said ealiar about cloning that specific code to add back button.
   */
  $destination = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $destination = substr($destination, 0, strripos($destination, '&'));

  $form['actions']['back'] = array(
    '#markup' => l(t('Back'), $destination),
    '#weight' => 20,
  );
  
  return $form;
}

/**
 * This function will set deafult theme for site and administration after form submission.
 * @param type $form
 * @param type $form_state
 */
function opin_theme_enable_form_submit(&$form, &$form_state) {
  global $install_state;
  
  $enable = array(
      'theme_default' => $form_state['values']['theme_list'],
      'admin_theme' => $form_state['values']['theme_admin_list'],
  );
  theme_enable($enable);
  variable_set('theme_default', $form_state['values']['theme_list']);
  variable_set('admin_theme', $form_state['values']['theme_admin_list']);
  theme_disable(array('bartik'));

  $install_state['parameters']['theme_enable'] = 'ok';
}

/**
 * This function will return global setting form for theme.
 * @param type $form
 * @param type $form_state
 * @param type $install_state
 * @return type
 */
function opin_theme_setting_form($form, &$form_state, &$install_state) {
    return opin_theme_form($form, $form_state, NULL);
}

/**
 * Adding extra function to set paramater and to add back button
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function opin_form_opin_theme_setting_form_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = '_theme_setting_set_install_parameter';
  
  //Same here cloning the code.
  $destination = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $destination = substr($destination, 0, strripos($destination, '&'));

  $form['actions']['back'] = array(
    '#markup' => l(t('Back'), $destination),
    '#weight' => 20,
  );
}

/**
 * Setting the paramater
 * @global array $install_state
 * @param type $form
 * @param type $form_state
 */
function _theme_setting_set_install_parameter($form, &$form_state) {
  global $install_state;
  $install_state['parameters']['theme_setting'] = 'theme_set';
}

/**
 * This function will show global setting form for theme. It is clone code from drupal itself, since calling theme setting form from drupal core is
 * not working here. See https://api.drupal.org/api/function/system_theme_settings/7 for details documentation.
 * @param type $form
 * @param type $form_state
 * @param type $key
 * @return string
 */
function opin_theme_form($form, &$form_state, $key = NULL) {
    // Default settings are defined in theme_get_setting() in includes/theme.inc
  if ($key) {
    $var = 'theme_' . $key . '_settings';
    $themes = list_themes();
    $features = $themes[$key]->info['features'];
  }
  else {
    $var = 'theme_settings';
  }

  $form['var'] = array(
    '#type' => 'hidden',
    '#value' => $var,
  );

  // Toggle settings
  $toggles = array(
    'logo' => t('Logo'),
    'name' => t('Site name'),
    'slogan' => t('Site slogan'),
    'node_user_picture' => t('User pictures in posts'),
    'comment_user_picture' => t('User pictures in comments'),
    'comment_user_verification' => t('User verification status in comments'),
    'favicon' => t('Shortcut icon'),
    'main_menu' => t('Main menu'),
    'secondary_menu' => t('Secondary menu'),
  );

  // Some features are not always available
  $disabled = array();
  if (!variable_get('user_pictures', 0)) {
    $disabled['toggle_node_user_picture'] = TRUE;
    $disabled['toggle_comment_user_picture'] = TRUE;
  }
  if (!module_exists('comment')) {
    $disabled['toggle_comment_user_picture'] = TRUE;
    $disabled['toggle_comment_user_verification'] = TRUE;
  }

  $form['theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Toggle display'),
    '#description' => t('Enable or disable the display of certain page elements.'),
  );
  foreach ($toggles as $name => $title) {
    if ((!$key) || in_array($name, $features)) {
      $form['theme_settings']['toggle_' . $name] = array(
        '#type' => 'checkbox',
        '#title' => $title,
        '#default_value' => theme_get_setting('toggle_' . $name, $key),
      );
      // Disable checkboxes for features not supported in the current configuration.
      if (isset($disabled['toggle_' . $name])) {
        $form['theme_settings']['toggle_' . $name]['#disabled'] = TRUE;
      }
    }
  }

  if (!element_children($form['theme_settings'])) {
    // If there is no element in the theme settings fieldset then do not show
    // it -- but keep it in the form if another module wants to alter.
    $form['theme_settings']['#access'] = FALSE;
  }

  // Logo settings
  if ((!$key) || in_array('logo', $features)) {
    $form['logo'] = array(
      '#type' => 'fieldset',
      '#title' => t('Logo image settings'),
      '#description' => t('If toggled on, the following logo will be displayed.'),
      '#attributes' => array('class' => array('theme-settings-bottom')),
    );
    $form['logo']['default_logo'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the default logo'),
      '#default_value' => theme_get_setting('default_logo', $key),
      '#tree' => FALSE,
      '#description' => t('Check here if you want the theme to use the logo supplied with it.'),
    );
    $form['logo']['settings'] = array(
      '#type' => 'container',
      '#states' => array(
        
        // Hide the logo settings when using the default logo.
        'invisible' => array(
          'input[name="default_logo"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['logo']['settings']['logo_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to custom logo'),
      '#description' => t('The path to the file you would like to use as your logo file instead of the default logo.'),
      '#default_value' => theme_get_setting('logo_path', $key),
    );
    $form['logo']['settings']['logo_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload logo image'),
      '#maxlength' => 40,
      '#description' => t("If you don't have direct file access to the server, use this field to upload your logo."),
    );
  }

  if ((!$key) || in_array('favicon', $features)) {
    $form['favicon'] = array(
      '#type' => 'fieldset',
      '#title' => t('Shortcut icon settings'),
      '#description' => t("Your shortcut icon, or 'favicon', is displayed in the address bar and bookmarks of most browsers."),
    );
    $form['favicon']['default_favicon'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the default shortcut icon.'),
      '#default_value' => theme_get_setting('default_favicon', $key),
      '#description' => t('Check here if you want the theme to use the default shortcut icon.'),
    );
    $form['favicon']['settings'] = array(
      '#type' => 'container',
      '#states' => array(
        
        // Hide the favicon settings when using the default favicon.
        'invisible' => array(
          'input[name="default_favicon"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['favicon']['settings']['favicon_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to custom icon'),
      '#description' => t('The path to the image file you would like to use as your custom shortcut icon.'),
      '#default_value' => theme_get_setting('favicon_path', $key),
    );
    $form['favicon']['settings']['favicon_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload icon image'),
      '#description' => t("If you don't have direct file access to the server, use this field to upload your shortcut icon."),
    );
  }

  // Inject human-friendly values for logo and favicon.
  foreach (array(
    'logo' => 'logo.png',
    'favicon' => 'favicon.ico',
  ) as $type => $default) {
    if (isset($form[$type]['settings'][$type . '_path'])) {
      $element = &$form[$type]['settings'][$type . '_path'];

      // If path is a public:// URI, display the path relative to the files
      // directory; stream wrappers are not end-user friendly.
      $original_path = $element['#default_value'];
      $friendly_path = NULL;
      if (file_uri_scheme($original_path) == 'public') {
        $friendly_path = file_uri_target($original_path);
        $element['#default_value'] = $friendly_path;
      }
    }
  }

  if ($key) {
    // Call engine-specific settings.
    $function = $themes[$key]->prefix . '_engine_settings';
    if (function_exists($function)) {
      $form['engine_specific'] = array(
        '#type' => 'fieldset',
        '#title' => t('Theme-engine-specific settings'),
        '#description' => t('These settings only exist for the themes based on the %engine theme engine.', array('%engine' => $themes[$key]->prefix)),
      );
      $function($form, $form_state);
    }

    // Create a list which includes the current theme and all its base themes.
    if (isset($themes[$key]->base_themes)) {
      $theme_keys = array_keys($themes[$key]->base_themes);
      $theme_keys[] = $key;
    }
    else {
      $theme_keys = array($key);
    }

    // Save the name of the current theme (if any), so that we can temporarily
    // override the current theme and allow theme_get_setting() to work
    // without having to pass the theme name to it.
    $default_theme = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : NULL;
    $GLOBALS['theme_key'] = $key;

    // Process the theme and all its base themes.
    foreach ($theme_keys as $theme) {
      // Include the theme-settings.php file.
      $filename = DRUPAL_ROOT . '/' . str_replace("/$theme.info", '', $themes[$theme]->filename) . '/theme-settings.php';
      if (file_exists($filename)) {
        require_once $filename;
      }

      // Call theme-specific settings.
      $function = $theme . '_form_system_theme_settings_alter';
      if (function_exists($function)) {
        $function($form, $form_state);
      }
    }

    // Restore the original current theme.
    if (isset($default_theme)) {
      $GLOBALS['theme_key'] = $default_theme;
    }
    else {
      unset($GLOBALS['theme_key']);
    }
  }

  $form = system_settings_form($form);
  // We don't want to call system_settings_form_submit(), so change #submit.
  array_pop($form['#submit']);
  $form['#submit'][] = 'opin_theme_settings_submit';
  $form['#validate'][] = 'opin_theme_settings_validate';
  return $form;
}

/**
 * Same here. This validate function is clone from drupal core. See https://api.drupal.org/api/function/system_theme_settings_validate/7 
 * for details documentaion
 * @param type $form
 * @param type $form_state
 */
function opin_theme_settings_validate($form, &$form_state) {
  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  $file = file_save_upload('logo_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['logo_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('logo_upload', t('The logo could not be uploaded.'));
    }
  }

  $validators = array('file_validate_extensions' => array('ico png gif jpg jpeg apng svg'));

  // Check for a new uploaded favicon.
  $file = file_save_upload('favicon_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['favicon_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('favicon_upload', t('The favicon could not be uploaded.'));
    }
  }

  // If the user provided a path for a logo or favicon file, make sure a file
  // exists at that path.
  if ($form_state['values']['logo_path']) {
    $path = _opin_theme_settings_validate_path($form_state['values']['logo_path']);
    if (!$path) {
      form_set_error('logo_path', t('The custom logo path is invalid.'));
    }
  }
  if ($form_state['values']['favicon_path']) {
    $path = _opin_theme_settings_validate_path($form_state['values']['favicon_path']);
    if (!$path) {
      form_set_error('favicon_path', t('The custom favicon path is invalid.'));
    }
  }
}

/**
 * This function will save the configuration. This is also clone from drupal core.
 * See https://api.drupal.org/api/function/system_theme_settings_submit/7 for details documentation.
 * @param type $form
 * @param type $form_state
 */
function opin_theme_settings_submit($form, &$form_state) {
  // Exclude unnecessary elements before saving.
  form_state_values_clean($form_state);

  $values = $form_state['values'];

  // Extract the name of the theme from the submitted form values, then remove
  // it from the array so that it is not saved as part of the variable.
  $key = $values['var'];
  unset($values['var']);

  // If the user uploaded a new logo or favicon, save it to a permanent location
  // and use it in place of the default theme-provided file.
  if ($file = $values['logo_upload']) {
    unset($values['logo_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_logo'] = 0;
    $values['logo_path'] = $filename;
    $values['toggle_logo'] = 1;
  }
  if ($file = $values['favicon_upload']) {
    unset($values['favicon_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_favicon'] = 0;
    $values['favicon_path'] = $filename;
    $values['toggle_favicon'] = 1;
  }

  // If the user entered a path relative to the system files directory for
  // a logo or favicon, store a public:// URI so the theme system can handle it.
  if (!empty($values['logo_path'])) {
    $values['logo_path'] = _opin_theme_settings_validate_path($values['logo_path']);
  }
  if (!empty($values['favicon_path'])) {
    $values['favicon_path'] = _opin_theme_settings_validate_path($values['favicon_path']);
  }

  if (empty($values['default_favicon']) && !empty($values['favicon_path'])) {
    $values['favicon_mimetype'] = file_get_mimetype($values['favicon_path']);
  }

  variable_set($key, $values);
  drupal_set_message(t('The configuration options have been saved.'));

  cache_clear_all();
}

/**
 * This function will validate path for file field. Its also clone from drupal core.
 * See https://api.drupal.org/api/drupal/modules%21system%21system.admin.inc/function/_system_theme_settings_validate_path/7 for details documentation
 * @param string $path
 * @return string|boolean
 */
function _opin_theme_settings_validate_path($path) {
  // Absolute local file paths are invalid.
  if (drupal_realpath($path) == $path) {
    return FALSE;
  }
  // A path relative to the Drupal root or a fully qualified URI is valid.
  if (is_file($path)) {
    return $path;
  }
  // Prepend 'public://' for relative file paths within public filesystem.
  if (file_uri_scheme($path) === FALSE) {
    $path = 'public://' . $path;
  }
  if (is_file($path)) {
    return $path;
  }
  return FALSE;
}